#ifndef CONFIGSINGLETON_H
#define CONFIGSINGLETON_H

#include <QSettings>
#include <QApplication>


// holding some configs that will store in non-volatile storage
class ConfigSingleton : public QSettings
{
public:
    static ConfigSingleton & instance()
    {
        static ConfigSingleton inst;
        return inst;
    }

private:
    ConfigSingleton()
        : QSettings(QString("%1/AutoId.ini").arg(QApplication::applicationDirPath()), QSettings::IniFormat)
    {
    }
};

#endif // CONFIGSINGLETON_H
