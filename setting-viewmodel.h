#ifndef SETTINGVIEWMODEL_H
#define SETTINGVIEWMODEL_H

#include <QObject>
#include <QString>
#include <QTranslator>


// view model for setting page
class SettingViewModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int langId READ langId WRITE setLangId NOTIFY langIdChanged)

    Q_PROPERTY(QString acDbAddress READ acDbAddress CONSTANT)
    Q_PROPERTY(int acDbPort READ acDbPort CONSTANT)
    Q_PROPERTY(QString acDbUsername READ acDbUsername CONSTANT)
    Q_PROPERTY(QString acDbPassword READ acDbPassword CONSTANT)
    Q_PROPERTY(QString acDbDbname READ acDbDbname CONSTANT)
    Q_PROPERTY(bool acDbConnected READ acDbConnected NOTIFY acDbConnectedChanged)

    Q_PROPERTY(QString dbAddress READ dbAddress CONSTANT)
    Q_PROPERTY(int dbPort READ dbPort CONSTANT)
    Q_PROPERTY(QString dbUsername READ dbUsername CONSTANT)
    Q_PROPERTY(QString dbPassword READ dbPassword CONSTANT)
    Q_PROPERTY(QString dbDbname READ dbDbname CONSTANT)
    Q_PROPERTY(bool dbConnected READ dbConnected NOTIFY dbConnectedChanged)

public:
    enum Language
    {
        ZH_TW,
        ZH_CN,
        EN_US
    };

public:
    SettingViewModel(QObject * parent = nullptr);
    virtual ~SettingViewModel() {}

public:
    int langId() const { return langId_; }

    QString acDbAddress() const { return acDbAddress_; }
    int acDbPort() const { return acDbPort_; }
    QString acDbUsername() const { return acDbUsername_; }
    QString acDbPassword() const { return acDbPassword_; }
    QString acDbDbname() const { return acDbDbname_; }
    bool acDbConnected() const;

    QString dbAddress() const { return dbAddress_; }
    int dbPort() const { return dbPort_; }
    QString dbUsername() const { return dbUsername_; }
    QString dbPassword() const { return dbPassword_; }
    QString dbDbname() const { return dbDbname_; }
    bool dbConnected() const;

    void setLangId(int id);

public:
    Q_INVOKABLE bool restartAcDb();
    Q_INVOKABLE bool updateAcDb(const QString & address, const int port, const QString & username, const QString & password, const QString & dbname);
    Q_INVOKABLE QString acDbLastError() const;

    Q_INVOKABLE bool restartDb();
    Q_INVOKABLE bool updateDb(const QString & address, const int port, const QString & username, const QString & password, const QString & dbname);
    Q_INVOKABLE QString dbLastError() const;

private:
    void serializeLanguage();
    void deserializeLanguage();

    void serializeAcDatabase();
    void deserializeAcDatabase();

    void serializeDatabase();
    void deserializeDatabase();

private slots:
    void handleAcDbConnectedChanged();
    void handleDbConnectedChanged();

signals:
    void langIdChanged();

    void acDbConnectedChanged();
    void acDbStatusChanged();

    void dbConnectedChanged();
    void dbStatusChanged();

private:
    int langId_;
    QString acDbAddress_;
    int acDbPort_;
    QString acDbUsername_;
    QString acDbPassword_;
    QString acDbDbname_;
    QString dbAddress_;
    int dbPort_;
    QString dbUsername_;
    QString dbPassword_;
    QString dbDbname_;
    QTranslator * cnTranslator_;
    QTranslator * enTranslator_;
    QTranslator * currentTranslator_;
    bool inited_;
};

#endif // SETTINGVIEWMODEL_H
