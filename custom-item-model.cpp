#include "custom-item-model.h"

CustomItemModel::CustomItemModel(QObject * parent)
    : QStandardItemModel(parent)
{
    roleNames_[CustomItemModel::CustomId] =  "customId";
    roleNames_[CustomItemModel::CustomName] =  "customName";

    selectQuery_.prepare("select * from customlist");
}

void CustomItemModel::refresh()
{
    enum TableFields
    {
        Id, Name
    };

    clear();
    indexLookup_.clear();
    selectQuery_.exec();
    for (int i = 0; selectQuery_.next(); i++)
    {
        QStandardItem * item = new QStandardItem();
        QString id = selectQuery_.value(0).toString();
        QString name = selectQuery_.value(1).toString();
        item->setData(id, CustomItemModel::CustomId);
        item->setData(name, CustomItemModel::CustomName);

        appendRow(item);

        indexLookup_.insert(id, i);
    }
}

QVariantMap CustomItemModel::get(int index) const
{
    QVariantMap result;

    auto item = itemData(this->index(index, 0));
    QMapIterator<int, QVariant> iter(item);
    while(iter.hasNext())
    {
        iter.next();
        result.insert(roleNames_[iter.key()], iter.value());
    }

    return result;
}

int CustomItemModel::indexFromId(const QString & id) const
{
    auto it = indexLookup_.find(id);
    return it != indexLookup_.end() ? it.value() : -1;
}

bool CustomItemModel::insertDb(const QString & id, const QString & name)
{
    operateQuery_.prepare("insert into `customlist` (`Customid`, `Customname`) values (?, ?)");
    operateQuery_.bindValue(0, id);
    operateQuery_.bindValue(1, name);

    return operateQuery_.exec();
}

bool CustomItemModel::updateDb(const QString & id, const QString & newId, const QString & name)
{
    operateQuery_.prepare("update `customlist` set `Customid` = ?, `Customname` = ? where `Customid` = ?");
    operateQuery_.bindValue(2, id);
    operateQuery_.bindValue(0, newId);
    operateQuery_.bindValue(1, name);

    return operateQuery_.exec();
}

bool CustomItemModel::deleteDb(const QString & id)
{
    operateQuery_.prepare("delete from `customlist` where `Customid` = ?");
    operateQuery_.bindValue(0, id);

    return operateQuery_.exec();
}

QString CustomItemModel::lastDbError() const
{
    return operateQuery_.lastError().text();
}
