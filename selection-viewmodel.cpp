#include "selection-viewmodel.h"
#include <QStandardItemModel>
#include <QtSql>
#include "custom-item-model.h"
#include "staff-item-model.h"


namespace  {

class RecordItemModel : public QStandardItemModel
{
public:
    enum RoleNames
    {
        Rid = Qt::UserRole + 1, BN, PR, Len, Id, Customid, Notes,
    };

public:
    RecordItemModel(QObject * parent);
    virtual ~RecordItemModel() {}

public:
    virtual QHash<int, QByteArray> roleNames() const { return roleNames_; }
    QVariantMap getRecord(int index) const;
    void refresh();

private:
    QHash<int, QByteArray> roleNames_;
    QSqlQuery selectQuery_;
};

RecordItemModel::RecordItemModel(QObject * parent)
    : QStandardItemModel(parent)
{
    roleNames_[RecordItemModel::Rid] =  "rid";
    roleNames_[RecordItemModel::BN] =  "bn";
    roleNames_[RecordItemModel::PR] =  "pr";
    roleNames_[RecordItemModel::Len] =  "len";
    roleNames_[RecordItemModel::Id] =  "id";
    roleNames_[RecordItemModel::Customid] =  "customid";
    roleNames_[RecordItemModel::Notes] =  "notes";

    selectQuery_.prepare("select * from record group by pr");

    refresh();
}

QVariantMap RecordItemModel::getRecord(int index) const
{
    QVariantMap result;

    auto item = itemData(this->index(index, 0));
    QMapIterator<int, QVariant> iter(item);
    while(iter.hasNext())
    {
        iter.next();
        result.insert(roleNames_[iter.key()], iter.value());
    }

    return result;
}

void RecordItemModel::refresh()
{
    clear();

    enum TableFields
    {
        Rid, Mid, BN, PR, Startdate, Enddate, Len, Id, Customid, Notes
    };

    selectQuery_.exec();
    while (selectQuery_.next())
    {
        QStandardItem * item = new QStandardItem();
        item->setData(selectQuery_.value(Rid).toInt(), RecordItemModel::Rid);
        item->setData(selectQuery_.value(BN).toString(), RecordItemModel::BN);
        item->setData(selectQuery_.value(PR).toString(), RecordItemModel::PR);
        item->setData(selectQuery_.value(Len).toDouble(), RecordItemModel::Len);
        item->setData(selectQuery_.value(Id).toInt(), RecordItemModel::Id);
        item->setData(selectQuery_.value(Customid).toString(), RecordItemModel::Customid);
        item->setData(selectQuery_.value(Notes).toString(), RecordItemModel::Notes);
        appendRow(item);
    }
}

}


// =====================================================

SelectionViewModel::SelectionViewModel(QObject * parent)
    : QObject(parent)
    , recordModel_(new RecordItemModel(this))
    , customModel_(new CustomItemModel(this))
    , staffModel_(new StaffItemModel(this))
{
}

QAbstractItemModel * SelectionViewModel::recordModel() const
{
    return recordModel_;
}

QAbstractItemModel * SelectionViewModel::customModel() const
{
    return customModel_;
}

QAbstractItemModel * SelectionViewModel::staffModel() const
{
    return staffModel_;
}

void SelectionViewModel::refresh()
{
    static_cast<RecordItemModel *>(recordModel_)->refresh();
    static_cast<CustomItemModel *>(customModel_)->refresh();
    static_cast<StaffItemModel *>(staffModel_)->refresh();
}

QVariantMap SelectionViewModel::getRecord(int index) const
{
    return static_cast<RecordItemModel *>(recordModel_)->getRecord(index);
}

bool SelectionViewModel::saveRecord(const QVariantMap & data)
{
    QSqlQuery sql;
    sql.prepare("insert into record (`BN`, `PR`, `Len`, `Id`, `Customid`, `Notes`) values (:bn, :pr, :len, :id, :customid, :notes)");
    sql.bindValue(":bn", data.value("bn").toString());
    sql.bindValue(":pr", data.value("pr").toString());
    sql.bindValue(":len", data.value("len").toDouble());
    sql.bindValue(":id", data.value("staffId").toInt());
    sql.bindValue(":customid", data.value("customId").toString());
    sql.bindValue(":notes", data.value("notes").toString());

    return sql.exec();
}

void SelectionViewModel::sortRecords(int role, int order)
{
    recordModel_->setSortRole(RecordItemModel::BN + role);
    recordModel_->sort(0, static_cast<Qt::SortOrder>(order));
}
