#include "ac-database-singleton.h"

AcDatabaseSingleton & AcDatabaseSingleton::instance()
{
    static AcDatabaseSingleton inst(QSqlDatabase::addDatabase("QMYSQL", "ACCOUNT"));
    return inst;
}

AcDatabaseSingleton::AcDatabaseSingleton(const QSqlDatabase & other)
    : DatabaseSingleton(other)
{
}
