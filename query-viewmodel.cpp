#include "query-viewmodel.h"
#include <QStandardItemModel>
#include <QVariant>
#include <QtSql>


namespace {

class DefectRecordItemModel : public QStandardItemModel
{
public:
    enum RoleName {
        RecordId = Qt::UserRole + 1, DefectId, Xposition, Yposition, Zposition, DefectType, Area, Width, Length, GroupId
    };

public:
    DefectRecordItemModel(QObject * parent);

public:
    virtual QHash<int, QByteArray> roleNames() const { return roleNames_; }
    QVariantMap getDefectRecord(int index) const;

    void queryByRecordId(int recordId);

private:
    void update();

private:
    QHash<int, QByteArray> roleNames_;
};


DefectRecordItemModel::DefectRecordItemModel(QObject * parent)
    : QStandardItemModel(parent)
{
    roleNames_[DefectRecordItemModel::RecordId] =  "recordid";
    roleNames_[DefectRecordItemModel::DefectId] =  "defectid";
    roleNames_[DefectRecordItemModel::Xposition] =  "xposition";
    roleNames_[DefectRecordItemModel::Yposition] =  "yposition";
    roleNames_[DefectRecordItemModel::Zposition] =  "zposition";
    roleNames_[DefectRecordItemModel::DefectType] =  "defecttype";
    roleNames_[DefectRecordItemModel::Area] =  "area";
    roleNames_[DefectRecordItemModel::Width] =  "width";
    roleNames_[DefectRecordItemModel::Length] =  "length";
    roleNames_[DefectRecordItemModel::GroupId] =  "groupid";
}

QVariantMap DefectRecordItemModel::getDefectRecord(int index) const
{
    QVariantMap result;

    auto item = itemData(this->index(index, 0));
    QMapIterator<int, QVariant> iter(item);
    while(iter.hasNext())
    {
        iter.next();
        result.insert(roleNames_[iter.key()], iter.value());
    }

    return result;
}

void DefectRecordItemModel::queryByRecordId(int recordId)
{
    enum Fields
    {
        Did, Rid, Defectid, Location, Size, X_axis, Z_axis, X_size, Y_size, Level, Groupid, Filename, DefectTypeName
    };

    QSqlQuery query;
    query.prepare("select drecord.*, defecttype.Defectname from drecord left join defecttype on drecord.Defectid = defecttype.Defectid where drecord.Rid = ?");
    query.bindValue(0, recordId);
    query.exec();
    this->clear();
    while (query.next())
    {
        QStandardItem * item = new QStandardItem();
        item->setData(query.value(Rid).toInt(), DefectRecordItemModel::RecordId);
        item->setData(query.value(Defectid).toInt(), DefectRecordItemModel::DefectId);
        item->setData(query.value(X_axis).toDouble(), DefectRecordItemModel::Xposition);
        item->setData(query.value(Location).toDouble(), DefectRecordItemModel::Yposition);
        item->setData(query.value(Z_axis).toDouble(), DefectRecordItemModel::Zposition);
        item->setData(query.value(DefectTypeName).toString(), DefectRecordItemModel::DefectType);
        item->setData(query.value(Size).toDouble(), DefectRecordItemModel::Area);
        item->setData(query.value(X_size).toDouble(), DefectRecordItemModel::Width);
        item->setData(query.value(Y_size).toDouble(), DefectRecordItemModel::Length);
        item->setData(query.value(Groupid).toInt(), DefectRecordItemModel::GroupId);
        this->appendRow(item);
    }
}

void DefectRecordItemModel::update()
{
    QSqlQuery query;
    query.exec("select drecord.*, defecttype.Defectname from drecord left join defecttype on drecord.Defectid = defecttype.Defectid");
}

}

// =====================================================

QueryViewModel::QueryViewModel(QObject * parent) :
    QObject(parent),
    defectRecordModel_(new DefectRecordItemModel(this))
{
}

QAbstractItemModel * QueryViewModel::defectRecordModel() const
{
    return defectRecordModel_;
}

QVariantMap QueryViewModel::getRecord(int recordId) const
{
    QVariantMap result;

    QSqlQuery query;
    query.prepare("select * from record where Rid = ?");
    query.bindValue(0, recordId);
    query.exec();
    if (query.next())
    {
        result.insert("rid", query.value(0).toInt());
        result.insert("mid", query.value(1).toInt());
        result.insert("bn", query.value(2).toString());
        result.insert("pr", query.value(3).toString());
        result.insert("startdate", query.value(4).toDateTime());
        result.insert("enddate", query.value(5).toDateTime());
        result.insert("len", query.value(6).toDouble());
        result.insert("id", query.value(7).toInt());
        result.insert("customid", query.value(8).toString());
        result.insert("notes", query.value(9).toString());
    }

    return result;
}

QVariantMap QueryViewModel::getDefectRecord(int index) const
{
    return static_cast<DefectRecordItemModel *>(defectRecordModel_)->getDefectRecord(index);
}

QVariantList QueryViewModel::queryRecordProducts(const QDateTime & from, const QDateTime & to)
{
    QVariantList result;

    QSqlQuery query;
    query.prepare("select `PR` from record where Startdate >= ? and Startdate <= ? group by `PR`");
    query.bindValue(0, from);
    query.bindValue(1, to);
    query.exec();
    while (query.next())
    {
        QString item = query.value(0).toString();
        result.push_back(item);
    }

    return result;
}

QVariantList QueryViewModel::queryRecords(const QString & product, const QDateTime & from, const QDateTime & to)
{
    QVariantList result;

    QSqlQuery query;
    query.prepare("select * from record where `Startdate` >= ? and `Startdate` <= ? and `PR` = ?");
    query.bindValue(0, from);
    query.bindValue(1, to);
    query.bindValue(2, product);
    query.exec();
    while (query.next())
    {
        QVariantMap item;
        item.insert("BN", query.value(2).toString());
        item.insert("Rid", query.value(0).toString());
        item.insert("StartDate", query.value(4).toDateTime());
        result.push_back(item);
    }

    return result;
}

void QueryViewModel::queryDefectRecords(int recordId)
{
    static_cast<DefectRecordItemModel *>(defectRecordModel_)->queryByRecordId(recordId);
}

void QueryViewModel::clearDefectRecords()
{
    static_cast<DefectRecordItemModel *>(defectRecordModel_)->clear();
}

QVariantMap QueryViewModel::queryChartData(double fromY, double toY)
{
    // note: struct of return value
    // { minX, maxX, LT: [] }

    QVariantMap result;
    result.insert("minX", 0);
    result.insert("maxX", 40);

    QVariantList pointsLT;
    for (int i = 0, l = defectRecordModel_->rowCount(); i < l; i++) {
        auto y = defectRecordModel_->data(defectRecordModel_->index(i, 0), DefectRecordItemModel::Yposition).toDouble();
        if (y >= fromY && y <= toY)
        {
            auto x = defectRecordModel_->data(defectRecordModel_->index(i, 0), DefectRecordItemModel::Xposition).toDouble();
            pointsLT.append(QPointF(x, y));
        }
    }
    result.insert("LT", pointsLT);

    return result;
}

QString QueryViewModel::updateRecordBn(int rid, const QString & bn)
{
    QSqlQuery query;
    query.prepare("update `record` set `BN` = ? where `Rid` = ?");
    query.bindValue(0, bn);
    query.bindValue(1, rid);
    return query.exec() ? "" : query.lastError().text();
}

QString QueryViewModel::deleteRecordBn(int rid)
{
    QSqlQuery query;
    query.prepare("delete from `record` where `Rid` = ?");
    query.bindValue(0, rid);
    return query.exec() ? "" : query.lastError().text();
}
