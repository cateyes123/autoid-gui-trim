#ifndef QUERYVIEWMODEL_H
#define QUERYVIEWMODEL_H

#include <QVariantMap>
#include <QObject>
#include <QString>

class QAbstractItemModel;
class QStandardItemModel;


// view model for query page
class QueryViewModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QAbstractItemModel * defectRecordModel READ defectRecordModel CONSTANT)

public:
    QueryViewModel(QObject * parent = nullptr);
    virtual ~QueryViewModel() {}

public:
    QAbstractItemModel * defectRecordModel() const;
    Q_INVOKABLE QVariantMap getRecord(int recordId) const;
    Q_INVOKABLE QVariantMap getDefectRecord(int index) const;

    Q_INVOKABLE QVariantList queryRecordProducts(const QDateTime & from, const QDateTime & to);
    Q_INVOKABLE QVariantList queryRecords(const QString & product, const QDateTime & from, const QDateTime & to);
    Q_INVOKABLE void queryDefectRecords(int recordId);
    Q_INVOKABLE void clearDefectRecords();
    Q_INVOKABLE QVariantMap queryChartData(double fromY, double toY);

    Q_INVOKABLE QString updateRecordBn(int rid, const QString & bn);
    Q_INVOKABLE QString deleteRecordBn(int rid);

private:
    QStandardItemModel * defectRecordModel_;
};

#endif // QUERYVIEWMODEL_H

