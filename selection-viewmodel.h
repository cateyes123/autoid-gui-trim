#ifndef SELECTIONVIEWMODEL_H
#define SELECTIONVIEWMODEL_H

#include <QObject>
#include <QVariantMap>

class QAbstractItemModel;
class QStandardItemModel;

// view model for selection page
class SelectionViewModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QAbstractItemModel * recordModel READ recordModel CONSTANT)
    Q_PROPERTY(QAbstractItemModel * customModel READ customModel CONSTANT)
    Q_PROPERTY(QAbstractItemModel * staffModel READ staffModel CONSTANT)

public:
    SelectionViewModel(QObject * parent = nullptr);
    virtual ~SelectionViewModel() {}

public:
    QAbstractItemModel * recordModel() const;
    QAbstractItemModel * customModel() const;
    QAbstractItemModel * staffModel() const;

    Q_INVOKABLE void refresh();
    Q_INVOKABLE QVariantMap getRecord(int index) const;
    Q_INVOKABLE bool saveRecord(const QVariantMap & data);
    Q_INVOKABLE void sortRecords(int role, int order);

signals:
    void staffModelChanged();

private:
    QStandardItemModel * recordModel_;
    QStandardItemModel * customModel_;
    QStandardItemModel * staffModel_;
};

#endif // SELECTIONVIEWMODEL_H
