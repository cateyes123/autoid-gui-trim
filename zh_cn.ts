<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh">
<context>
    <name>ControlBar</name>
    <message>
        <location filename="qml/ControlBar.qml" line="22"/>
        <source>即時瑕疵資料</source>
        <translation>实时瑕疵数据</translation>
    </message>
    <message>
        <location filename="qml/ControlBar.qml" line="45"/>
        <source>驗布資料選擇</source>
        <translation>验布数据选择</translation>
    </message>
    <message>
        <location filename="qml/ControlBar.qml" line="68"/>
        <source>檢測履歷查詢</source>
        <translation>检测履历查询</translation>
    </message>
    <message>
        <location filename="qml/ControlBar.qml" line="86"/>
        <source>設定</source>
        <translation>设定</translation>
    </message>
</context>
<context>
    <name>CustomTable</name>
    <message>
        <location filename="qml/selection-page/CustomTable.qml" line="10"/>
        <source>客戶代號</source>
        <translation>客户代号</translation>
    </message>
    <message>
        <location filename="qml/selection-page/CustomTable.qml" line="15"/>
        <source>客戶名稱</source>
        <translation>客户名称</translation>
    </message>
</context>
<context>
    <name>CustomTableManager</name>
    <message>
        <location filename="qml/selection-page/CustomTableManager.qml" line="13"/>
        <source>客戶管理</source>
        <translation>客户管理</translation>
    </message>
    <message>
        <location filename="qml/selection-page/CustomTableManager.qml" line="17"/>
        <source>客戶代號</source>
        <translation>客户代号</translation>
    </message>
    <message>
        <location filename="qml/selection-page/CustomTableManager.qml" line="44"/>
        <location filename="qml/selection-page/CustomTableManager.qml" line="54"/>
        <location filename="qml/selection-page/CustomTableManager.qml" line="65"/>
        <source>失敗</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="qml/selection-page/CustomTableManager.qml" line="102"/>
        <source>關閉</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="qml/selection-page/CustomTableManager.qml" line="139"/>
        <source>客戶代號 (不可重複)</source>
        <translation>客户代号 (不可重复)</translation>
    </message>
    <message>
        <location filename="qml/selection-page/CustomTableManager.qml" line="147"/>
        <source>客戶名稱</source>
        <translation>客户名称</translation>
    </message>
    <message>
        <location filename="qml/selection-page/CustomTableManager.qml" line="166"/>
        <location filename="qml/selection-page/CustomTableManager.qml" line="168"/>
        <source>新增</source>
        <translation>新增</translation>
    </message>
    <message>
        <location filename="qml/selection-page/CustomTableManager.qml" line="175"/>
        <location filename="qml/selection-page/CustomTableManager.qml" line="177"/>
        <source>修改</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="qml/selection-page/CustomTableManager.qml" line="184"/>
        <location filename="qml/selection-page/CustomTableManager.qml" line="186"/>
        <source>刪除</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>DefectChart</name>
    <message>
        <location filename="qml/DefectChart.qml" line="77"/>
        <source>左上</source>
        <translation>左上</translation>
    </message>
    <message>
        <location filename="qml/DefectChart.qml" line="86"/>
        <source>右上</source>
        <translation>右上</translation>
    </message>
    <message>
        <location filename="qml/DefectChart.qml" line="95"/>
        <source>左下</source>
        <translation>左下</translation>
    </message>
    <message>
        <location filename="qml/DefectChart.qml" line="104"/>
        <source>右下</source>
        <translation>右下</translation>
    </message>
</context>
<context>
    <name>DefectTable</name>
    <message>
        <location filename="qml/query-page/DefectTable.qml" line="10"/>
        <source>檢測面</source>
        <translation>检测面</translation>
    </message>
    <message>
        <location filename="qml/query-page/DefectTable.qml" line="15"/>
        <source>位置Y</source>
        <translation>位置Y</translation>
    </message>
    <message>
        <location filename="qml/query-page/DefectTable.qml" line="20"/>
        <source>橫向位置X</source>
        <translation>横向位置X</translation>
    </message>
    <message>
        <location filename="qml/query-page/DefectTable.qml" line="25"/>
        <source>瑕疵類型</source>
        <translation>瑕疵类型</translation>
    </message>
    <message>
        <location filename="qml/query-page/DefectTable.qml" line="30"/>
        <source>尺寸(mm^2)</source>
        <translation>尺寸(mm^2)</translation>
    </message>
    <message>
        <location filename="qml/query-page/DefectTable.qml" line="35"/>
        <source>寬度(mm)</source>
        <translation>宽度(mm)</translation>
    </message>
    <message>
        <location filename="qml/query-page/DefectTable.qml" line="40"/>
        <source>長度(mm)</source>
        <translation>长度(mm)</translation>
    </message>
    <message>
        <location filename="qml/query-page/DefectTable.qml" line="45"/>
        <source>組數</source>
        <translation>组数</translation>
    </message>
</context>
<context>
    <name>FaceRadioBox</name>
    <message>
        <location filename="qml/query-page/FaceRadioBox.qml" line="16"/>
        <source>檢測面</source>
        <translation>检测面</translation>
    </message>
    <message>
        <location filename="qml/query-page/FaceRadioBox.qml" line="49"/>
        <source>正面</source>
        <translation>正面</translation>
    </message>
    <message>
        <location filename="qml/query-page/FaceRadioBox.qml" line="78"/>
        <source>反面</source>
        <translation>反面</translation>
    </message>
</context>
<context>
    <name>OverallTable</name>
    <message>
        <location filename="qml/realtime-page/OverallTable.qml" line="13"/>
        <source>檢測面</source>
        <translation>检测面</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/OverallTable.qml" line="18"/>
        <source>位置Y</source>
        <translation>位置Y</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/OverallTable.qml" line="23"/>
        <source>橫向位置X</source>
        <translation>横向位置X</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/OverallTable.qml" line="28"/>
        <source>瑕疵類型</source>
        <translation>瑕疵类型</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/OverallTable.qml" line="33"/>
        <source>尺寸(mm^2)</source>
        <translation>尺寸(mm^2)</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/OverallTable.qml" line="38"/>
        <source>寬度(mm)</source>
        <translation>宽度(mm)</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/OverallTable.qml" line="43"/>
        <source>長度(mm)</source>
        <translation>长度(mm)</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/OverallTable.qml" line="48"/>
        <source>組數</source>
        <translation>组数</translation>
    </message>
</context>
<context>
    <name>Popup</name>
    <message>
        <location filename="qml/Popup.qml" line="55"/>
        <source>提示</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="qml/Popup.qml" line="64"/>
        <source>關閉</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="qml/Popup.qml" line="120"/>
        <source>確認</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="qml/Popup.qml" line="133"/>
        <source>取消</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>RecordBnManager</name>
    <message>
        <location filename="qml/query-page/RecordBnManager.qml" line="14"/>
        <source>批號管理</source>
        <translation>批号管理</translation>
    </message>
    <message>
        <location filename="qml/query-page/RecordBnManager.qml" line="30"/>
        <location filename="qml/query-page/RecordBnManager.qml" line="40"/>
        <source>失敗</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="qml/query-page/RecordBnManager.qml" line="77"/>
        <source>關閉</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="qml/query-page/RecordBnManager.qml" line="96"/>
        <source>批號</source>
        <translation>批号</translation>
    </message>
    <message>
        <location filename="qml/query-page/RecordBnManager.qml" line="115"/>
        <location filename="qml/query-page/RecordBnManager.qml" line="117"/>
        <source>修改</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="qml/query-page/RecordBnManager.qml" line="124"/>
        <location filename="qml/query-page/RecordBnManager.qml" line="126"/>
        <source>刪除</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>RecordTable</name>
    <message>
        <location filename="qml/selection-page/RecordTable.qml" line="9"/>
        <source>批號</source>
        <translation>批号</translation>
    </message>
    <message>
        <location filename="qml/selection-page/RecordTable.qml" line="14"/>
        <source>產品</source>
        <translation>产品</translation>
    </message>
    <message>
        <location filename="qml/selection-page/RecordTable.qml" line="19"/>
        <source>寬幅</source>
        <translation>宽幅</translation>
    </message>
    <message>
        <location filename="qml/selection-page/RecordTable.qml" line="24"/>
        <source>操作員</source>
        <translation>操作员</translation>
    </message>
    <message>
        <location filename="qml/selection-page/RecordTable.qml" line="29"/>
        <source>客戶名稱</source>
        <translation>客户名称</translation>
    </message>
    <message>
        <location filename="qml/selection-page/RecordTable.qml" line="34"/>
        <source>備註</source>
        <translation>备注</translation>
    </message>
</context>
<context>
    <name>SettingMenu</name>
    <message>
        <location filename="qml/SettingMenu.qml" line="36"/>
        <location filename="qml/SettingMenu.qml" line="58"/>
        <location filename="qml/SettingMenu.qml" line="70"/>
        <source>帳號資料庫錯誤</source>
        <translation>账号数据库错误</translation>
    </message>
    <message>
        <location filename="qml/SettingMenu.qml" line="43"/>
        <location filename="qml/SettingMenu.qml" line="52"/>
        <location filename="qml/SettingMenu.qml" line="65"/>
        <source>內容資料庫錯誤</source>
        <translation>内容数据库错误</translation>
    </message>
    <message>
        <location filename="qml/SettingMenu.qml" line="109"/>
        <source>設定</source>
        <translation>设定</translation>
    </message>
    <message>
        <location filename="qml/SettingMenu.qml" line="118"/>
        <source>關閉</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="qml/SettingMenu.qml" line="136"/>
        <source>多國語言</source>
        <translation>多国语言</translation>
    </message>
    <message>
        <location filename="qml/SettingMenu.qml" line="143"/>
        <source>語言</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="qml/SettingMenu.qml" line="153"/>
        <source>確定</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="qml/SettingMenu.qml" line="181"/>
        <source>內容資料庫</source>
        <translation>内容数据库</translation>
    </message>
    <message>
        <location filename="qml/SettingMenu.qml" line="170"/>
        <source>帳號資料庫</source>
        <oldsource>帳號</oldsource>
        <translation>账号数据库</translation>
    </message>
</context>
<context>
    <name>SettingMenuDbSecion</name>
    <message>
        <location filename="qml/SettingMenuDbSecion.qml" line="24"/>
        <source>位置</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="qml/SettingMenuDbSecion.qml" line="38"/>
        <source>阜</source>
        <translation>阜</translation>
    </message>
    <message>
        <location filename="qml/SettingMenuDbSecion.qml" line="50"/>
        <source>帳號</source>
        <translation>账号</translation>
    </message>
    <message>
        <location filename="qml/SettingMenuDbSecion.qml" line="65"/>
        <source>密碼</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="qml/SettingMenuDbSecion.qml" line="77"/>
        <source>資料庫名稱</source>
        <translation>数据库名称</translation>
    </message>
    <message>
        <location filename="qml/SettingMenuDbSecion.qml" line="87"/>
        <source>更新 / 重連</source>
        <translation>更新 / 重连</translation>
    </message>
</context>
<context>
    <name>StaffTable</name>
    <message>
        <location filename="qml/selection-page/StaffTable.qml" line="10"/>
        <source>員工代號</source>
        <translation>员工代号</translation>
    </message>
    <message>
        <location filename="qml/selection-page/StaffTable.qml" line="15"/>
        <source>員工名稱</source>
        <translation>员工名称</translation>
    </message>
    <message>
        <location filename="qml/selection-page/StaffTable.qml" line="20"/>
        <source>員工部門</source>
        <translation>员工部门</translation>
    </message>
</context>
<context>
    <name>StaffTableManager</name>
    <message>
        <location filename="qml/selection-page/StaffTableManager.qml" line="13"/>
        <source>員工管理</source>
        <translation>员工管理</translation>
    </message>
    <message>
        <location filename="qml/selection-page/StaffTableManager.qml" line="17"/>
        <source>員工代號</source>
        <translation>员工代号</translation>
    </message>
    <message>
        <location filename="qml/selection-page/StaffTableManager.qml" line="47"/>
        <location filename="qml/selection-page/StaffTableManager.qml" line="57"/>
        <location filename="qml/selection-page/StaffTableManager.qml" line="68"/>
        <source>失敗</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="qml/selection-page/StaffTableManager.qml" line="105"/>
        <source>關閉</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="qml/selection-page/StaffTableManager.qml" line="142"/>
        <source>員工名稱</source>
        <translation>员工名称</translation>
    </message>
    <message>
        <location filename="qml/selection-page/StaffTableManager.qml" line="150"/>
        <source>員工部門</source>
        <translation>员工部门</translation>
    </message>
    <message>
        <location filename="qml/selection-page/StaffTableManager.qml" line="169"/>
        <location filename="qml/selection-page/StaffTableManager.qml" line="171"/>
        <source>新增</source>
        <translation>新增</translation>
    </message>
    <message>
        <location filename="qml/selection-page/StaffTableManager.qml" line="178"/>
        <location filename="qml/selection-page/StaffTableManager.qml" line="180"/>
        <source>修改</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="qml/selection-page/StaffTableManager.qml" line="187"/>
        <location filename="qml/selection-page/StaffTableManager.qml" line="189"/>
        <source>刪除</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>TopPanel</name>
    <message>
        <location filename="qml/realtime-page/TopPanel.qml" line="39"/>
        <source>停止</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/TopPanel.qml" line="39"/>
        <source>啟動</source>
        <translation>启动</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/TopPanel.qml" line="55"/>
        <source>批號</source>
        <translation>批号</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/TopPanel.qml" line="64"/>
        <source>米長</source>
        <translation>米长</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/TopPanel.qml" line="73"/>
        <source>線速度(m/min)</source>
        <translation>线速度(m/min)</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/TopPanel.qml" line="82"/>
        <source>產品</source>
        <translation>产品</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/TopPanel.qml" line="91"/>
        <source>剩餘長度</source>
        <translation>剩余长度</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/TopPanel.qml" line="100"/>
        <source>預計完成時間</source>
        <translation>预计完成时间</translation>
    </message>
</context>
<context>
    <name>WebcamArea</name>
    <message>
        <location filename="qml/realtime-page/WebcamArea.qml" line="30"/>
        <source>攝影機狀態</source>
        <translation>摄像头状态</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="qml/main.qml" line="12"/>
        <source>AutoID</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>mainPage</name>
    <message>
        <location filename="qml/login-page/mainPage.qml" line="41"/>
        <source>登入</source>
        <translation>登入</translation>
    </message>
    <message>
        <location filename="qml/login-page/mainPage.qml" line="50"/>
        <source>帳號</source>
        <translation>账号</translation>
    </message>
    <message>
        <location filename="qml/login-page/mainPage.qml" line="63"/>
        <source>密碼</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="qml/login-page/mainPage.qml" line="74"/>
        <source>確認</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="qml/login-page/mainPage.qml" line="90"/>
        <source>帳號密碼錯誤</source>
        <translation>账号密码错误</translation>
    </message>
    <message>
        <location filename="qml/login-page/mainPage.qml" line="91"/>
        <source>請聯絡管理員</source>
        <translation>请联络管理员</translation>
    </message>
    <message>
        <location filename="qml/login-page/mainPage.qml" line="95"/>
        <location filename="qml/realtime-page/mainPage.qml" line="65"/>
        <source>資料庫錯誤</source>
        <translation>数据库错误</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="142"/>
        <source>起~</source>
        <translation>起~</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="151"/>
        <source>迄</source>
        <translation>迄</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="200"/>
        <location filename="qml/selection-page/mainPage.qml" line="68"/>
        <source>批號</source>
        <translation>批号</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="179"/>
        <location filename="qml/selection-page/mainPage.qml" line="78"/>
        <source>產品</source>
        <translation>产品</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="271"/>
        <source>日期</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="278"/>
        <source>數量</source>
        <translation>数量</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="162"/>
        <source>查詢</source>
        <translation>查询</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="326"/>
        <source>瑕疵檢出位置Y/-Y(m)</source>
        <translation>瑕疵检出位置Y/-Y(m)</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="338"/>
        <source>區域</source>
        <translation>区域</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="345"/>
        <location filename="qml/realtime-page/mainPage.qml" line="292"/>
        <source>瑕疵類型</source>
        <translation>瑕疵类型</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="352"/>
        <source>瑕疵尺寸(mm^2)</source>
        <translation>瑕疵尺寸(mm^2)</translation>
    </message>
    <message>
        <location filename="qml/query-page/mainPage.qml" line="369"/>
        <location filename="qml/selection-page/mainPage.qml" line="257"/>
        <source>離開頁面</source>
        <translation>离开页面</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/mainPage.qml" line="173"/>
        <source>檢測長度</source>
        <translation>瑕疵长度</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/mainPage.qml" line="180"/>
        <source>檢測X軸座標</source>
        <translation>检测X轴坐标</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/mainPage.qml" line="187"/>
        <location filename="qml/realtime-page/mainPage.qml" line="300"/>
        <source>瑕疵面積</source>
        <translation>瑕疵面积</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/mainPage.qml" line="194"/>
        <location filename="qml/realtime-page/mainPage.qml" line="316"/>
        <source>瑕疵長度</source>
        <translation>瑕疵长度</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/mainPage.qml" line="201"/>
        <location filename="qml/realtime-page/mainPage.qml" line="324"/>
        <source>瑕疵寬度</source>
        <translation>瑕疵宽度</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/mainPage.qml" line="284"/>
        <source>組數</source>
        <translation>组数</translation>
    </message>
    <message>
        <location filename="qml/realtime-page/mainPage.qml" line="308"/>
        <source>橫向位置</source>
        <translation>横向位置</translation>
    </message>
    <message>
        <location filename="qml/selection-page/mainPage.qml" line="88"/>
        <source>幅寬</source>
        <translation>幅宽</translation>
    </message>
    <message>
        <location filename="qml/selection-page/mainPage.qml" line="95"/>
        <source>操作員</source>
        <translation>操作员</translation>
    </message>
    <message>
        <location filename="qml/selection-page/mainPage.qml" line="123"/>
        <source>備註</source>
        <translation>备注</translation>
    </message>
    <message>
        <location filename="qml/selection-page/mainPage.qml" line="145"/>
        <source>廠商</source>
        <translation>厂商</translation>
    </message>
    <message>
        <location filename="qml/selection-page/mainPage.qml" line="192"/>
        <source>歷史紀錄</source>
        <translation>历史纪录</translation>
    </message>
    <message>
        <location filename="qml/selection-page/mainPage.qml" line="237"/>
        <source>確認執行</source>
        <translation>确认执行</translation>
    </message>
    <message>
        <location filename="qml/selection-page/mainPage.qml" line="248"/>
        <source>請選擇廠商</source>
        <translation>请选择厂商</translation>
    </message>
</context>
</TS>
