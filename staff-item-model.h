#ifndef STAFFITEMMODEL_H
#define STAFFITEMMODEL_H

#include <QStandardItemModel>
#include <QMap>
#include <QtSql>


// representing the stafflist table in database
class StaffItemModel : public QStandardItemModel
{
    Q_OBJECT

public:
    enum RoleNames
    {
        StaffId = Qt::UserRole + 1, StaffName, StaffGroupId, ComboBoxDisplay
    };

public:
    StaffItemModel(QObject * parent);
    virtual ~StaffItemModel() {}

public:
    virtual QHash<int, QByteArray> roleNames() const { return roleNames_; }
    Q_INVOKABLE void refresh();
    Q_INVOKABLE QVariantMap get(int index) const;
    Q_INVOKABLE int indexFromId(int id) const;
    Q_INVOKABLE bool insertDb(const QString & name, const QString & groupId);
    Q_INVOKABLE bool updateDb(int id, const QString & name, const QString & groupId);
    Q_INVOKABLE bool deleteDb(int id);
    Q_INVOKABLE QString lastDbError() const;

private:
    QHash<int, QByteArray> roleNames_;
    QSqlQuery selectQuery_;
    QSqlQuery operateQuery_;
    QMap<int, int> indexLookup_;
};

#endif // STAFFITEMMODEL_H
