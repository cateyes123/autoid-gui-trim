#ifndef CUSTOMITEMMODEL_H
#define CUSTOMITEMMODEL_H

#include <QStandardItemModel>
#include <QMap>
#include <QtSql>


// representing the customlist table in database
class CustomItemModel : public QStandardItemModel
{
    Q_OBJECT

public:
    enum RoleNames
    {
        CustomId = Qt::UserRole + 1, CustomName,
    };

public:
    CustomItemModel(QObject * parent);
    virtual ~CustomItemModel() {}

public:
    virtual QHash<int, QByteArray> roleNames() const { return roleNames_; }
    Q_INVOKABLE void refresh();
    Q_INVOKABLE QVariantMap get(int index) const;
    Q_INVOKABLE int indexFromId(const QString & id) const;
    Q_INVOKABLE bool insertDb(const QString & id, const QString & name);
    Q_INVOKABLE bool updateDb(const QString & id, const QString & newId, const QString & name);
    Q_INVOKABLE bool deleteDb(const QString & id);
    Q_INVOKABLE QString lastDbError() const;

private:
    QHash<int, QByteArray> roleNames_;
    QSqlQuery selectQuery_;
    QSqlQuery operateQuery_;
    QMap<QString, int> indexLookup_;
};

#endif // CUSTOMITEMMODEL_H
