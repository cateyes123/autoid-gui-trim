#ifndef NETWORKMONITOR_H
#define NETWORKMONITOR_H

#include <QObject>
#include <QRunnable>


struct NetworkResult
{
    NetworkResult() : tx(0), rx(0) {}

    double tx; // bytes/s
    double rx; // bytes/s
};

//Q_DECLARE_METATYPE(NetworkResult)

// =====================================================

class NetworkWorker : public QObject, public QRunnable
{
    Q_OBJECT

signals:
    void done(const NetworkResult &);

public:
    NetworkWorker(int seconds, QObject * parent = nullptr);
    virtual ~NetworkWorker() {}

protected:
    virtual void run() override;

private:
    int seconds_;
};


// =====================================================

// provide the status of network
class NetworkMonitor : public QObject
{
    Q_OBJECT

signals:
    void updated(const NetworkResult &);

public:
    NetworkMonitor(QObject * parent = NULL);
    virtual ~NetworkMonitor() {}

public:
    bool starting() const;

    void start(int seconds);
    void stop();

private:
    void startWorker();

private slots:
    void handleWorkerDone(const NetworkResult & result);

private:
    bool starting_;
    int interval_sec_;
    NetworkWorker * worker_;
};

#endif // NETWORKMONITOR_H
