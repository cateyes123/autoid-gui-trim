#include "network-monitor.h"

#include <QProcess>
#include <QThreadPool>
#include <QDebug>


namespace {

#if defined(WIN32)

#include <thread>
#include <chrono>

NetworkResult profileNetworkTraffic(int seconds)
{
    struct helper_t {
        void operator()(NetworkResult & result)
        {
            QProcess process;
            process.start(QString("netstat -e"));
            process.waitForFinished();
            QString output(process.readAllStandardOutput());
            process.close();

            auto tokens = output.split("\n", QString::SkipEmptyParts);
            if (tokens.size() > 5)
            {
                QString statistics = tokens[4];
                tokens = statistics.split(" ", QString::SkipEmptyParts);
                if (tokens.size() >= 3)
                {
                    result.rx = tokens[1].toDouble();
                    result.tx = tokens[2].toDouble();
                }
            }
        }
    } helper;

    NetworkResult start, end, result;

    helper(start);
    std::this_thread::sleep_for(std::chrono::seconds(seconds));
    helper(end);
    result.rx = end.rx - start.rx;
    result.tx = end.tx - start.tx;

    return result;
}

#elif defined(__OSX__) || defined(Q_OS_MACOS)

NetworkResult profileNetworkTraffic(int seconds)
{
    NetworkResult result;

    QProcess process;
    process.start(QString("netstat -w%1 -I en0").arg(seconds));
    process.waitForFinished(seconds * 1000 + 200);
    QString output(process.readAllStandardOutput());
    process.close();

    auto tokens = output.split("\n", QString::SkipEmptyParts);
    if (!tokens.isEmpty())
    {
        QString statistics = tokens.last();
        tokens = statistics.split(" ", QString::SkipEmptyParts);
        if (tokens.size() > 5) {
            result.rx = tokens[2].toDouble();
            result.tx = tokens[5].toDouble();
        }
    }

    return result;
}

#elif defined(__linux__) || defined(Q_OS_LINUX)

NetworkResult profileNetworkTraffic(int seconds)
{
    NetworkResult result;

    QProcess process;
    QString pipeCmd = QString("sar -n DEV %1 1 | grep eth0").arg(seconds);
    process.start("bash", QStringList() << "-c" << pipeCmd);
    process.waitForFinished(seconds * 1000 + 400);
    QString output(process.readAllStandardOutput());
    process.close();

    auto tokens = output.split("\n", QString::SkipEmptyParts);
    if (!tokens.isEmpty())
    {
        QString statistics = tokens.last();
        tokens = statistics.split(" ", QString::SkipEmptyParts);
        if (tokens.size() > 6) {
            result.rx = tokens[4].toDouble() * 1024;
            result.tx = tokens[5].toDouble() * 1024;
        }
    }

    return result;
}

#endif

}


// =====================================================

NetworkWorker::NetworkWorker(int seconds, QObject * parent)
    : QObject (parent)
    , seconds_(seconds)
{
    // register struct to be qml components
    qRegisterMetaType<NetworkResult>("NetworkResult");
}

void NetworkWorker::run()
{
    NetworkResult result = profileNetworkTraffic(seconds_);
    emit done(result);
}


// =====================================================

NetworkMonitor::NetworkMonitor(QObject * parent)
    : QObject(parent)
    , starting_(false)
    , interval_sec_(1)
    , worker_(nullptr)
{
}

bool NetworkMonitor::starting() const
{
    return starting_;
}

void NetworkMonitor::start(int seconds)
{
    Q_ASSERT(seconds > 0);

    if (!starting())
    {
        starting_ = true;
        interval_sec_ = seconds;

        startWorker();
    }
}

void NetworkMonitor::stop()
{
    starting_ = false;
}

void NetworkMonitor::startWorker()
{
    // note: QThreadPool takes ownership and deletes runnable automatically
    QThreadPool::globalInstance()->start(worker_ = new NetworkWorker(interval_sec_));
    Q_ASSERT(worker_->autoDelete());
    QObject::connect(worker_, SIGNAL(done(const NetworkResult &)), this, SLOT(handleWorkerDone(const NetworkResult &)));
}

void NetworkMonitor::handleWorkerDone(const NetworkResult & result)
{
    if (starting())
    {
        startWorker();
        emit updated(result);
    }
}
