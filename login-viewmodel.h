#ifndef LOGINVIEWMODEL_H
#define LOGINVIEWMODEL_H

#include <QObject>
#include <QString>


// view model for login page
class LoginViewModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString username READ username WRITE setUsername);

public:
    virtual ~LoginViewModel() {}

public:
    void setUsername(const QString & username);
    QString username() const;

public:
    Q_INVOKABLE bool verify(const QString & username, const QString & password);
    Q_INVOKABLE bool dbConnected() const;
    Q_INVOKABLE QString dbLastError() const;
};

#endif // LOGINVIEWMODEL_H
