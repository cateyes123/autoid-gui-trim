QT += widgets quick sql
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    network-monitor.cpp \
    setting-viewmodel.cpp \
    database-singleton.cpp \
    query-viewmodel.cpp \
    selection-viewmodel.cpp \
    realtime-viewmodel.cpp \
    login-viewmodel.cpp \
    staff-item-model.cpp \
    custom-item-model.cpp \
    ac-database-singleton.cpp \
    thirdparty/qaesencryption.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    realtime-viewmodel.h \
    network-monitor.h \
    database-singleton.h \
    config-singleton.h \
    query-viewmodel.h \
    selection-viewmodel.h \
    setting-viewmodel.h \
    login-viewmodel.h \
    staff-item-model.h \
    custom-item-model.h \
    ac-database-singleton.h \
    thirdparty/qaesencryption.h

TRANSLATIONS = zh_tw.ts zh_cn.ts en_us.ts
