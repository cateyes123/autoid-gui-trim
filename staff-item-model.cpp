#include "staff-item-model.h"

StaffItemModel::StaffItemModel(QObject * parent)
    : QStandardItemModel(parent)
{
    roleNames_[StaffItemModel::StaffId] =  "staffId";
    roleNames_[StaffItemModel::StaffName] =  "staffName";
    roleNames_[StaffItemModel::StaffGroupId] =  "staffGroupId";
    roleNames_[StaffItemModel::ComboBoxDisplay] =  "modelData";

    selectQuery_.prepare("select * from stafflist");
}

void StaffItemModel::refresh()
{
    enum TableFields
    {
        Id, Name, GroupId
    };

    clear();
    indexLookup_.clear();
    selectQuery_.exec();
    for (int i = 0; selectQuery_.next(); i++)
    {
        QStandardItem * item = new QStandardItem();
        QString id = selectQuery_.value(0).toString();
        QString name = selectQuery_.value(1).toString();
        QString groupid = selectQuery_.value(2).toString();
        QString displayText = QString("%1. %2 [%3]").arg(id, name, groupid);
        item->setData(id, StaffItemModel::StaffId);
        item->setData(name, StaffItemModel::StaffName);
        item->setData(groupid, StaffItemModel::StaffGroupId);
        item->setData(displayText, StaffItemModel::ComboBoxDisplay);

        appendRow(item);

        indexLookup_.insert(selectQuery_.value(0).toInt(), i);
    }
}

QVariantMap StaffItemModel::get(int index) const
{
    QVariantMap result;

    auto item = itemData(this->index(index, 0));
    QMapIterator<int, QVariant> iter(item);
    while(iter.hasNext())
    {
        iter.next();
        result.insert(roleNames_[iter.key()], iter.value());
    }

    return result;
}

int StaffItemModel::indexFromId(int id) const
{
    auto it = indexLookup_.find(id);
    return it != indexLookup_.end() ? it.value() : -1;
}

bool StaffItemModel::insertDb(const QString & name, const QString & groupId)
{
    operateQuery_.prepare("insert into `stafflist` (`Id`, `Username`, `Groupid`) values (NULL, ?, ?)");
    operateQuery_.bindValue(0, name);
    operateQuery_.bindValue(1, groupId);

    return operateQuery_.exec();
}

bool StaffItemModel::updateDb(int id, const QString & name, const QString & groupId)
{
    operateQuery_.prepare("update `stafflist` set `Username` = ?, `Groupid` = ? where `Id` = ?");
    operateQuery_.bindValue(2, id);
    operateQuery_.bindValue(0, name);
    operateQuery_.bindValue(1, groupId);

    return operateQuery_.exec();
}

bool StaffItemModel::deleteDb(int id)
{
    operateQuery_.prepare("delete from `stafflist` where `Id` = ?");
    operateQuery_.bindValue(0, id);

    return operateQuery_.exec();
}

QString StaffItemModel::lastDbError() const
{
    return operateQuery_.lastError().text();
}
