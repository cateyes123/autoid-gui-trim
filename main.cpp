#include <QApplication>
#include <QQmlApplicationEngine>
#include "login-viewmodel.h"
#include "setting-viewmodel.h"
#include "realtime-viewmodel.h"
#include "selection-viewmodel.h"
#include "query-viewmodel.h"
#include "config-singleton.h"


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);
    QApplication app(argc, argv);

    // register classes to be qml components
    qmlRegisterType<SettingViewModel>("com.autoid", 1, 0, "SettingViewModel");
    qmlRegisterType<LoginViewModel>("com.autoid", 1, 0, "LoginViewModel");
    qmlRegisterType<RealtimeViewModel>("com.autoid", 1, 0, "RealtimeViewModel");
    qmlRegisterType<SelectionViewModel>("com.autoid", 1, 0, "SelectionViewModel");
    qmlRegisterType<QueryViewModel>("com.autoid", 1, 0, "QueryViewModel");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
