#ifndef ACDATABASESINGLETON_H
#define ACDATABASESINGLETON_H

#include <QObject>
#include <QtSql>
#include <database-singleton.h>


// just like DatabaseSingleton but dedicated to database for login accounts
class AcDatabaseSingleton : public DatabaseSingleton
{
public:
    static AcDatabaseSingleton & instance();

private:
    AcDatabaseSingleton() = delete;
    AcDatabaseSingleton(const QSqlDatabase & other);
};

#endif // ACDATABASESINGLETON_H
