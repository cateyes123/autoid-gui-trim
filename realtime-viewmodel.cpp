#include "realtime-viewmodel.h"

#include <functional>
#include <QStandardItemModel>
#include <QVariant>
#include <QtSql>

namespace {

// a inner class that is representing the drecord table in database
class DefectRecordItemModel : public QStandardItemModel
{
public:
    enum RoleName {
        Xposition = Qt::UserRole + 1, Yposition, Zposition, DefectType, Area, Width, Length, GroupId, Filename
    };

public:
    DefectRecordItemModel(std::function<void(const QString &)> emitSqlFailedCallback, QObject * parent);

public:
    virtual QHash<int, QByteArray> roleNames() const { return roleNames_; }
    QVariantMap getDefectRecord(int index) const;

    void refresh();
    QVariantMap queryChartData(double fromY, double toY);
    void reset();

private:
    bool openDb();
    QString defectType(int defectId);

    void emitSqlFailed(const QString & reason);

private:
    std::function<void(const QString &)> emitSqlFailedCallback_;
    QHash<int, QByteArray> roleNames_;
    QSqlDatabase realtimeDb_;
    QMap<int, QString> defectTypeList_;
    int currentRecordCount_;
};


DefectRecordItemModel::DefectRecordItemModel(std::function<void(const QString &)> emitSqlFailedCallback, QObject * parent)
    : QStandardItemModel(parent)
    , emitSqlFailedCallback_(emitSqlFailedCallback)
    , realtimeDb_(QSqlDatabase::addDatabase("QSQLITE", "SQLITE"))
    , currentRecordCount_(0)
{
    roleNames_[DefectRecordItemModel::Xposition] = "xposition";
    roleNames_[DefectRecordItemModel::Yposition] = "yposition";
    roleNames_[DefectRecordItemModel::Zposition] = "zposition";
    roleNames_[DefectRecordItemModel::DefectType] = "defecttype";
    roleNames_[DefectRecordItemModel::Area] = "area";
    roleNames_[DefectRecordItemModel::Width] = "width";
    roleNames_[DefectRecordItemModel::Length] = "length";
    roleNames_[DefectRecordItemModel::GroupId] = "groupid";
    roleNames_[DefectRecordItemModel::Filename] = "filename";
}

QVariantMap DefectRecordItemModel::getDefectRecord(int index) const
{
    QVariantMap result;

    auto item = itemData(this->index(index, 0));
    QMapIterator<int, QVariant> iter(item);
    while(iter.hasNext())
    {
        iter.next();
        result.insert(roleNames_[iter.key()], iter.value());
    }

    return result;
}

void DefectRecordItemModel::refresh()
{
    if (!realtimeDb_.isOpen())
    {
        openDb();
    }

    enum Fields
    {
        Z_axis, Location, X_axis, Defectid, Size, X_size, Y_size, Groupid, Filename
    };

    if (realtimeDb_.isOpen())
    {
        QSqlQuery query(realtimeDb_);
        QString sql = QString("select * from drecord limit -1 offset %1").arg(currentRecordCount_);
        if (query.exec(sql))
        {
            while (query.next())
            {
                QStandardItem * item = new QStandardItem();

                item->setData(query.value(Z_axis).toDouble(), DefectRecordItemModel::Zposition);
                item->setData(query.value(X_axis).toDouble(), DefectRecordItemModel::Xposition);
                item->setData(query.value(Location).toDouble(), DefectRecordItemModel::Yposition);
                item->setData(query.value(Size).toDouble(), DefectRecordItemModel::Area);
                item->setData(query.value(X_size).toDouble(), DefectRecordItemModel::Width);
                item->setData(query.value(Y_size).toDouble(), DefectRecordItemModel::Length);
                item->setData(query.value(Groupid).toInt(), DefectRecordItemModel::GroupId);
                item->setData(query.value(Filename).toString(), DefectRecordItemModel::Filename);

                auto dtype = defectType(query.value(Defectid).toInt());
                item->setData(dtype, DefectRecordItemModel::DefectType);

                this->appendRow(item);
                currentRecordCount_++;
            }
        }
        else
        {
            emitSqlFailedCallback_(realtimeDb_.lastError().text());
        }
    }
}

QVariantMap DefectRecordItemModel::queryChartData(double fromY, double toY)
{
    // note: struct of return value
    // { minX, maxX, LT: [] }

    if (!realtimeDb_.isOpen())
    {
        openDb();
    }

    QVariantMap result;
    if (realtimeDb_.isOpen())
    {
        result.insert("minX", 0);
        result.insert("maxX", 40);

        QSqlQuery query(realtimeDb_);
        query.prepare("select x_axis, location from drecord where location >= ? and location <= ?");
        query.bindValue(0, fromY);
        query.bindValue(1, toY);

        if (query.exec())
        {
            QVariantList pointsLT;
            while (query.next())
            {
                auto x = query.value(0).toDouble();
                auto y = query.value(1).toDouble();
                pointsLT.append(QPointF(x, y));
            }
            result.insert("LT", pointsLT);
        }
        else
        {
            emitSqlFailedCallback_(realtimeDb_.lastError().text());
        }
    }

    return result;
}

void DefectRecordItemModel::reset()
{
    clear();
    currentRecordCount_ = 0;
}

bool DefectRecordItemModel::openDb()
{
    static QString dbpath = qApp->applicationDirPath() + QDir::separator() + "realtime.db";
    realtimeDb_.setDatabaseName(dbpath);
    bool opened = realtimeDb_.open();
    if (!opened)
    {
        emitSqlFailedCallback_(realtimeDb_.lastError().text());
    }

    return opened;
}

QString DefectRecordItemModel::defectType(int defectId)
{
    if (defectTypeList_.empty())
    {
        QSqlQuery query("select defectid, defectname from defecttype");
        while (query.next())
        {
            auto key = query.value(0).toInt();
            auto value = query.value(1).toString();
            defectTypeList_.insert(key, value);
        }
    }

    QString result;

    auto it = defectTypeList_.find(defectId);
    if (it != defectTypeList_.end())
    {
        result = it.value();
    }

    return result;
}

}


// =====================================================

RealtimeViewModel::RealtimeViewModel(QObject * parent)
    : QObject(parent)
    , drecordModel_(new DefectRecordItemModel(std::bind(&RealtimeViewModel::emitSqlFailed, this, std::placeholders::_1), this))
    , networkMonitor_(new NetworkMonitor(this))
{
    QObject::connect(networkMonitor_, SIGNAL(updated(const NetworkResult &)), this, SLOT(handleNetworkMonitorUpdated(const NetworkResult &)));
    networkMonitor_->start(1);
}

QAbstractItemModel * RealtimeViewModel::drecordModel() const
{
    return drecordModel_;
}

QString RealtimeViewModel::defectImgFolder() const
{
    static const QString path = qApp->applicationDirPath() + QDir::separator() + "Defects";
    return path;
}

QVariantMap RealtimeViewModel::getDefectRecord(int index) const
{
    return static_cast<DefectRecordItemModel *>(drecordModel_)->getDefectRecord(index);
}

QVariantMap RealtimeViewModel::queryChartData(double fromY, double toY)
{
    return static_cast<DefectRecordItemModel *>(drecordModel_)->queryChartData(fromY, toY);
}

void RealtimeViewModel::refreshDb()
{
    static_cast<DefectRecordItemModel *>(drecordModel_)->refresh();
}

void RealtimeViewModel::clearDb()
{
    static_cast<DefectRecordItemModel *>(drecordModel_)->reset();
}

void RealtimeViewModel::handleNetworkMonitorUpdated(const NetworkResult & result)
{
    networkResult_ = result;
    emit networkStatusUpdated();
}
