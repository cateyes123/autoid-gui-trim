import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    implicitHeight: 40

    property alias label: textInput.label
    property alias labelHAlignment: textInput.labelHAlignment
    property alias text: textInput.text
    property alias textFocus: textInput.textFocus
    property alias minimumDate: calendar.minimumDate
    property alias maximumDate: calendar.maximumDate

    function getDate() {
        var d = text.split("/");
        return new Date(+d[0], +d[1]-1, +d[2]);
    }

    LabelTextInput {
        id: textInput
        anchors.fill: parent
    }

    Calendar {
        id: calendar
        anchors.top: textInput.top
        anchors.left: textInput.right
        anchors.leftMargin: 10
        visible: textInput.textFocus

        function textFromDate(date) {
            var month = date.getMonth() + 1;
            month = month < 10 ? "0" + month : "" + month;
            var day = date.getDate();
            day = day < 10 ? "0" + day : "" + day;
            var text = date.getFullYear() + " / " + month + " / " + day;
            return text;
        }

        function assignDate(date) {
            textInput.text = textFromDate(date);
            textInput.textFocus = false;
        }

        onClicked: {
            assignDate(date);
        }

        Component.onCompleted: {
            assignDate(selectedDate);
        }
    }
}
