import QtQuick 2.0

Column {
    id: root

    spacing: 2
    property alias label: label.text
    property alias labelHAlignment: label.horizontalAlignment
    property alias text: input.text
    property alias textFocus: input.focus
    property alias readOnly: input.readOnly
    property alias maximumLength: input.maximumLength
    property alias inputHeight: input.height
    property alias echoMode: input.echoMode
    property alias inputItem: input

    onFocusChanged: if (focus) input.focus = focus;

    Text {
        id: label
        anchors.left: parent.left
        anchors.right: parent.right
        horizontalAlignment: Text.AlignLeft

        text: "Label"
        font.pixelSize: 12
        font.bold: true
        color: "dimgray"
    }

    TextInput {
        id: input
        anchors.left: parent.left
        anchors.right: parent.right
        font.pixelSize: 16
        horizontalAlignment: Text.AlignHCenter
        wrapMode: TextInput.Wrap

        Rectangle {
            anchors.fill: parent
            z: -1
            border.color: "dimgray"
        }
    }
}
