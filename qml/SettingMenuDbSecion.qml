import QtQuick 2.0

SettingMenuSecion {
    id: root

    property bool dbConnected: false

    signal updateButtonPressed(string address, int port, string username, string password, string dbname);

    function setData(address, port, username, password, dbname) {
        dbAddress.text = address;
        dbPort.text = port;
        dbUsername.text = username;
        dbPassword.text = password;
        dbDbname.text = dbname;
    }

    LabelTextInput {
        id: dbAddress
        anchors.top: parent.top
        anchors.topMargin: 20
        width: 260
        height: 40
        label: qsTr("位置")
        text: "localhost"
        focus: true
        KeyNavigation.tab: dbPort
    }

    LabelTextInput {
        id: dbPort
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.left: dbAddress.right
        anchors.leftMargin: 50
        width: 260
        height: 40
        label: qsTr("阜")
        text: "3306"

        KeyNavigation.tab: dbUsername
    }

    LabelTextInput {
        id: dbUsername
        anchors.top: dbAddress.bottom
        anchors.topMargin: 20
        width: 260
        height: 40
        label: qsTr("帳號")
        text: "root"

        KeyNavigation.tab: dbPassword
    }

    LabelTextInput {
        id: dbPassword
        anchors.top: dbPort.bottom
        anchors.topMargin: 20
        anchors.left: dbAddress.right
        anchors.leftMargin: 50
        width: 260
        height: 40
        echoMode: TextInput.Password
        label: qsTr("密碼")
        text: "1234"

        KeyNavigation.tab: dbDbname
    }

    LabelTextInput {
        id: dbDbname
        anchors.top: dbUsername.bottom
        anchors.topMargin: 20
        width: 260
        height: 40
        label: qsTr("資料庫名稱")
        text: "autoid"
    }

    IconButton {
        id: dbUpdate
        anchors.top: dbPassword.bottom
        anchors.topMargin: 20
        anchors.left: dbAddress.right
        anchors.leftMargin: 50
        text: qsTr("更新 / 重連")
        width: 260
        height: 40
        backgroundColor: root.dbConnected ? "lightgreen" : "red"

        onPressed: root.updateButtonPressed(dbAddress.text,
                                            +dbPort.text,
                                            dbUsername.text,
                                            dbPassword.text,
                                            dbDbname.text);
    }
}
