import QtQuick 2.0
import QtQuick.Controls 1.4


TableView {
    selectionMode: SelectionMode.SingleSelection

    TableViewColumn {
        role: "zposition"
        title: qsTr("檢測面")
        width: 100
    }
    TableViewColumn {
        role: "yposition"
        title: qsTr("位置Y")
        width: 100
    }
    TableViewColumn {
        role: "xposition"
        title: qsTr("橫向位置X")
        width: 100
    }
    TableViewColumn {
        role: "defecttype"
        title: qsTr("瑕疵類型")
        width: 100
    }
    TableViewColumn {
        role: "area"
        title: qsTr("尺寸(mm^2)")
        width: 100
    }
    TableViewColumn {
        role: "width"
        title: qsTr("寬度(mm)")
        width: 100
    }
    TableViewColumn {
        role: "length"
        title: qsTr("長度(mm)")
        width: 100
    }
    TableViewColumn {
        role: "groupid"
        title: qsTr("組數")
        width: 100
    }
}
