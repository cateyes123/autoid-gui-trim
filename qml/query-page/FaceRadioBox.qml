import QtQuick 2.0
import QtQuick.Controls 1.4

Column {
    id: root
    property int optionIndex: 0

    spacing: 2

    Text {
        id: label
        anchors.left: parent.left
        anchors.right: parent.right
        horizontalAlignment: Text.AlignLeft

        text: qsTr("檢測面")
        font.pixelSize: 12
        font.bold: true
        color: "dimgray"
    }

    Row {
        anchors.left: parent.left
        anchors.right: parent.right
        height: 16
        spacing: 4

        Rectangle {
            width: parent.height
            height: width
            radius: width / 2
            border.width: 1

            Rectangle {
                visible: root.optionIndex == 0
                anchors.centerIn: parent
                width: parent.width / 2
                height: width
                radius: width / 2
                color: "black"
            }
        }

        Text {
            width: 50
            height: parent.height
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 14
            text: "0." + qsTr("正面")
        }

        Item {
            width: 4
            height: 1
        }

        Rectangle {
            width: parent.height - 2
            height: width
            radius: width / 2
            border.width: 1

            Rectangle {
                visible: root.optionIndex == 1
                anchors.centerIn: parent
                width: parent.width / 2
                height: width
                radius: width / 2
                color: "black"
            }
        }

        Text {
            width: 50
            height: parent.height
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 14
            text: "1." + qsTr("反面")
        }
    }
}
