import QtQuick 2.0
import com.autoid 1.0
import "../"

Page {
    id: root

    function show(data) {
        root.locking = true;
    }

    function resetUi() {
        recordDate.text = "";
        viewModel.clearDefectRecords();
        defectChart.reset();
        defectRecordGroup.reset();
    }

    function showDefectRecord(defectRecord) {
        defectLocationInput.text = defectRecord.yposition;
        defectTestface.optionIndex = defectRecord.zposition;
        defectGroupidInput.text = defectRecord.groupid;
        defectTypeInput.text = defectRecord.defecttype;
        defectAreaInput.text = defectRecord.area;
        updateDrecordChart(defectRecord);
    }

    function updateDrecordChart(defectRecord) {
        var fromY = +defectRecord.yposition - 10;
        var toY = +defectRecord.yposition + 10;
        var result = viewModel.queryChartData(fromY, toY);
        defectChart.setPoints("LT", result.LT);
        defectChart.minY = fromY;
        defectChart.maxY = toY;
        defectChart.setHLPoint(+defectRecord.xposition, +defectRecord.yposition);
    }

    function performQueryButton() {
        var c = recordCondtionsGroup.getQueryConditions();
        var products = viewModel.queryRecordProducts(c.startDate, c.endDate);
        recordPrCombobox.setProducts(products);
        if (products.length === 0) {
            recordBnCombobox.setRecords([]);
            root.resetUi();
        }
    }

    Connections {
        target: recordQueryButton
        onClicked: root.performQueryButton();
    }

    Connections {
        target: recordPrCombobox
        onSelected: {
            var c = recordCondtionsGroup.getQueryConditions();
            var records = viewModel.queryRecords(product, c.startDate, c.endDate);
            recordBnCombobox.setRecords(records);
            if (records.length === 0) {
                root.resetUi();
            }
        }
    }

    Connections {
        target: recordBnCombobox
        onSelected: {
            defectTable.currentRow = -1;
            defectTable.selection.clear();
            root.resetUi();
            recordDate.text = textFromDate(record.StartDate);
            viewModel.queryDefectRecords(record.Rid);
        }

        function textFromDate(date) {
            var month = date.getMonth() + 1;
            month = month < 10 ? "0" + month : "" + month;
            var day = date.getDate();
            day = day < 10 ? "0" + day : "" + day;
            var text = date.getFullYear() + " / " + month + " / " + day;
            return text;
        }
    }

    QueryViewModel {
        id: viewModel
    }

    Section {
        id: defectTableSection
        anchors.top: parent.top
        anchors.left: recordCondtionsGroup.right
        anchors.bottom: parent.bottom
        anchors.right: defectRecordGroup.left

        DefectTable {
            id: defectTable
            anchors.fill: parent
            anchors.margins: 8
            model: viewModel.defectRecordModel

            Connections {
                target: defectTable.selection
                onSelectionChanged: {
                    defectTable.selection.forEach(function (rowIndex) {
                        var drecord = viewModel.getDefectRecord(rowIndex);
                        if ("recordid" in drecord) {
                            root.showDefectRecord(drecord);
                        } else {
                            console.log("missing record", rowIndex);
                        }
                    })
                }
            }
        }
    }


    Section {
        id: recordCondtionsGroup
        anchors.top: parent.top
        anchors.left: parent.left
        width: 240
        height: 324

        function getQueryConditions() {
            var result = {};
            result.startDate = recordStartdateInput.getDate();
            result.endDate = recordEnddateInput.getDate();
            return result;
        }

        Column {
            anchors.fill: parent
            anchors.margins: 8
            spacing: 4

            LabelCalendar {
                id: recordStartdateInput
                width: parent.width
                height: 40
                label: qsTr("起~")
//                minimumDate: new Date(2016, 5, 1)
//                maximumDate: new Date(2016, 6, 31)
            }

            LabelCalendar {
                id: recordEnddateInput
                width: parent.width
                height: 40
                label: qsTr("迄")
//                minimumDate: new Date(2016, 5, 1)
//                maximumDate: new Date(2016, 6, 31)
            }

            Item { width: parent.width; height: 4 }

            IconButton {
                id: recordQueryButton
                width: parent.width
                height: 22
                text: qsTr("查詢")
                backgroundColor: "lightblue"
                icon: "../pics/glyphicons-28-search.png"

                onClicked: {
                    recordStartdateInput.textFocus = false;
                    recordEnddateInput.textFocus = false;
                }
            }

            Item { width: parent.width; height: 4 }


            LabelComboBox {
                id: recordPrCombobox
                width: parent.width
                height: 40
                label: qsTr("產品")
                options: []

                signal selected(string product);

                function setProducts(products) {
                    options = products;
                    optionIndex = -1;
                    optionIndex = 0;
                }

                onOptionIndexChanged: {
                    if (optionIndex >= 0)
                        selected(options[optionIndex]);
                }
            }

            LabelComboBox {
                id: recordBnCombobox
                width: parent.width
                height: 40
                label: qsTr("批號")
                options: []

                signal selected(var record);
                property var records: []


                function updateRecordBn(index, bn) {
                    var oldIndex = index;
                    records[index].BN = bn;
                    options[index] = bn;
                    options = options; // note: re-update options
                    optionIndex = -1;
                    optionIndex = oldIndex;
                }

                function deleteRecordBn(index) {
                    records.splice(index, 1);
                    options.splice(index, 1);
                    options = options; // note: re-update options
                    optionIndex = -1;
                    optionIndex = 0;
                }

                function setRecords(records) {
                    recordBnCombobox.records = records;
                    options = modelForCombox(records);
                    optionIndex = -1;
                    optionIndex = 0;
                }

                function modelForCombox(input) {
                    var result = [];
                    for (var i in input) {
                        result.push(input[i].BN);
                    }
                    return result;
                }

                onOptionIndexChanged: {
                    if (optionIndex in records) {
                        var record = records[optionIndex];
                        selected(records[optionIndex]);
                    }
                }

                Image {
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.topMargin: 4
                    anchors.rightMargin: 6
                    width: 12
                    height: width
                    fillMode: Image.PreserveAspectFit
                    source: "../pics/glyphicons-281-settings.png"

                    MouseArea {
                        anchors.fill: parent
                        onPressed: {
                            if (recordBnCombobox.optionIndex in recordBnCombobox.records) {
                                var record = recordBnCombobox.records[recordBnCombobox.optionIndex];
                                recordBnManager.open(record.Rid, record.BN);
                            }
                        }
                    }
                }
            }

            LabelTextInput {
                id: recordDate
                width: parent.width
                label: qsTr("日期")
                readOnly: true
            }

            LabelTextInput {
                id: recordCount
                width: parent.width
                label: qsTr("數量")
                readOnly: true
                text: defectTable.rowCount
            }

        }
    }

    Section {
        anchors.top: recordCondtionsGroup.bottom
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        width: recordCondtionsGroup.width

        DefectChart {
            id: defectChart
            anchors.fill: parent
            anchors.margins: -10

            minX: 0
            maxX: 10000
            minY: 0
            maxY: 20
        }
    }

    Section {
        id: defectRecordGroup
        anchors.top: parent.top
        anchors.bottom: buttonSection.top
        anchors.right: parent.right
        width: 240

        function reset() {
            defectLocationInput.text = "";
            defectTestface.optionIndex = 0;
            defectGroupidInput.text = "";
            defectTypeInput.text = "";
            defectAreaInput.text = "";
        }

        Column {
            anchors.fill: parent
            anchors.margins: 8
            spacing: 4

            LabelTextInput {
                id: defectLocationInput
                label: qsTr("瑕疵檢出位置Y/-Y(m)")
                width: parent.width
                readOnly: true
            }

            FaceRadioBox {
                id: defectTestface
                width: parent.width
            }

            LabelTextInput {
                id: defectGroupidInput
                label: qsTr("區域")
                width: parent.width
                readOnly: true
            }

            LabelTextInput {
                id: defectTypeInput
                label: qsTr("瑕疵類型")
                width: parent.width
                readOnly: true
            }

            LabelTextInput {
                id: defectAreaInput
                label: qsTr("瑕疵尺寸(mm^2)")
                width: parent.width
                readOnly: true
            }
        }
    }

    Section {
        id: buttonSection
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        width: defectRecordGroup.width
        height: 40

        IconButton {
            anchors.fill: parent
            anchors.margins: 8
            text: qsTr("離開頁面")
            backgroundColor: "red"
            icon: "../pics/glyphicons-198-remove-circle.png"

            onPressed: {
                root.locking = false;
                root.changePageRequest("RealtimePage", null);
            }
        }
    }

    RecordBnManager {
        id: recordBnManager
        visible: false
        model: viewModel

        onClosed: visible = false;

        onEdited: {
            recordBnCombobox.updateRecordBn(recordBnCombobox.optionIndex, newBn);
        }

        onDeleted: {
            if (recordBnCombobox.options.length > 1) {
                recordBnCombobox.deleteRecordBn(recordBnCombobox.optionIndex);
            } else {
                root.performQueryButton();
            }
        }
    }
}
