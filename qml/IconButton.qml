import QtQuick 2.0

MouseArea {
    width: 120
    height: 40

    property alias icon: icon.source
    property alias text: text.text
    property alias textColor: text.color
    property alias textBold: text.font.bold
    property alias backgroundColor: background.color

    Item {
        anchors.centerIn: parent
        width: childrenRect.width

        Image {
            id: icon
            anchors.verticalCenter: parent.verticalCenter
            width: 16
            height: width
            fillMode: Image.PreserveAspectFit
            source: "pics/glyphicons-199-ok-circle.png"
        }

        Text {
            id: text
            anchors.left: icon.right
            anchors.leftMargin: 6
            anchors.verticalCenter: parent.verticalCenter
            text: "Button"
        }
    }

    Rectangle {
        id: background
        anchors.fill: parent
        z: -1
    }
}


