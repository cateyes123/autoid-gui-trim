import QtQuick 2.0

Item {
    width: parent.width
    height: 80
    anchors.left: parent.left
    anchors.leftMargin: 160

    property alias title: title.text
    property alias underlineVisible: underline.visible

    Text {
        id: title
        anchors.left: parent.left
        anchors.leftMargin: 20 - parent.anchors.leftMargin
        width: 40
        height: parent.height
        text: "Title"
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 16
        color: "black"
    }

    Rectangle {
        id: underline
        anchors.left: parent.left
        anchors.leftMargin: 20 - parent.anchors.leftMargin
        anchors.bottom: parent.bottom
        width: parent.width - 40
        height: 1
        color: "lightgray"
    }
}
