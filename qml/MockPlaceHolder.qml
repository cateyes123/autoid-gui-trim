import QtQuick 2.0

Rectangle {
    property alias text: description.text
    opacity: 0.8

    Text {
        id: description
        anchors.centerIn: parent
        horizontalAlignment: Text.Center
        verticalAlignment: Text.AlignVCenter
    }
}
