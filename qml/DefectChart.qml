import QtQuick 2.0
import QtCharts 2.2

ChartView {
    legend.visible: false
    antialiasing: true

    function reset() {
        axisX.min = 0;
        axisX.max = 1000;
        axisY.min = 0;
        axisY.max = 20;
        seriesHighlight.clear();
        seriesLT.clear();
        seriesLT.clear();
        seriesRB.clear();
        seriesRT.clear();
    }

    property alias minX: axisX.min
    property alias maxX: axisX.max
    property alias minY: axisY.min
    property alias maxY: axisY.max

    function setPoints(type, points) {
        var line = seriesLT;
        switch (type) {
        case "RT":
            line = seriesRT;
            break;
        case "LB":
            line = seriesLB;
            break;
        case "RB":
            line = seriesRB;
            break;
        }
        line.clear();
        for (var i = 0, l = points.length; i < l; i++) {
            line.append(points[i].x, points[i].y);
        }
    }

    function setHLPoint(x, y) {
        seriesHighlight.clear()
        seriesHighlight.append(x, y);
    }

    ValueAxis {
        id: axisX
        min: 0
        max: 100
        tickCount: 5
    }

    ValueAxis {
        id: axisY
        min: 0
        max: 20
        tickCount: 5
    }

    ScatterSeries {
        id: seriesHighlight
        axisX: axisX
        axisY: axisY
        color: "yellow"
        markerSize: 20
    }

    ScatterSeries {
        id: seriesLT
        axisX: axisX
        axisY: axisY
        color: "red"
        markerSize: 10
        name: qsTr("左上")
    }

    ScatterSeries {
        id: seriesRT
        axisX: axisX
        axisY: axisY
        color: "green"
        markerSize: 10
        name: qsTr("右上")
    }

    ScatterSeries {
        id: seriesLB
        axisX: axisX
        axisY: axisY
        color: "blue"
        markerSize: 10
        name: qsTr("左下")
    }

    ScatterSeries {
        id: seriesRB
        axisX: axisX
        axisY: axisY
        color: "gray"
        markerSize: 10
        name: qsTr("右下")
    }

    Component.onCompleted: {
        if (0) {
            var points = [];
            for (var i = 0; i <= 10; i++) {
                var point = Qt.point(Math.random() * maxX, Math.random() * maxY);
                points.push(point);
            }
            setPoints("LT", points);
        }
    }
}


