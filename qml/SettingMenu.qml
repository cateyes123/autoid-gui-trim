import QtQuick 2.0
import QtGraphicalEffects 1.0
import com.autoid 1.0


Rectangle {
    id: root
    color: Qt.rgba(0, 0, 0, 0.25)

    signal closed();
    signal tipsPopped(string message, string description);
    signal ready();

    property alias contentWidth: content.width
    property alias contentHeight: content.height

    function laodData() {
        acDbSection.setData(
                    viewModel.acDbAddress,
                    viewModel.acDbPort,
                    viewModel.acDbUsername,
                    viewModel.acDbPassword,
                    viewModel.acDbDbname);

        dbSection.setData(
                    viewModel.dbAddress,
                    viewModel.dbPort,
                    viewModel.dbUsername,
                    viewModel.dbPassword,
                    viewModel.dbDbname);
    }

    function restartAcDatabase(address, port, username, password, dbname) {
        viewModel.updateAcDb(address, port, username, password, dbname);
        if (!viewModel.acDbConnected) {
            tipsPopped(qsTr("帳號資料庫錯誤"), viewModel.acDbLastError());
        }
    }

    function restartDatabase(address, port, username, password, dbname) {
        viewModel.updateDb(address, port, username, password, dbname);
        if (!viewModel.dbConnected) {
            tipsPopped(qsTr("內容資料庫錯誤"), viewModel.dbLastError());
        }
    }

    SettingViewModel {
        id: viewModel

        onDbConnectedChanged: {
            if (!viewModel.dbConnected) {
                tipsPopped(qsTr("內容資料庫錯誤"), viewModel.dbLastError());
            }
        }

        onAcDbConnectedChanged: {
            if (!viewModel.acDbConnected) {
                tipsPopped(qsTr("帳號資料庫錯誤"), viewModel.acDbLastError());
            }
        }

        Component.onCompleted: {
            viewModel.restartDb();
            if (!viewModel.dbConnected) {
                tipsPopped(qsTr("內容資料庫錯誤"), viewModel.dbLastError());
            }

            viewModel.restartAcDb();
            if (!viewModel.acDbConnected) {
                tipsPopped(qsTr("帳號資料庫錯誤"), viewModel.acDbLastError());
            }

            root.ready();
        }
    }

    onVisibleChanged: {
        if (visible) {
            laodData();
        }
    }

    MouseArea {
        anchors.fill: parent
    }

    RectangularGlow {
        id: effect
        anchors.fill: content
        glowRadius: 10
        color: "#2f2f2f"
    }

    Rectangle {
        id: content
        anchors.centerIn: parent
        width: 800
        height: 600

        Column {
            anchors.fill: parent

            Item {
                id: titleGroup
                width: parent.width
                height: 50

                Text {
                    text: qsTr("設定")
                    anchors.centerIn: parent
                    font.pixelSize: 18
                    font.bold: true
                }

                IconButton {
                    width: 80
                    height: 30
                    text: qsTr("關閉")
                    icon: "pics/glyphicons-198-remove-circle.png"
                    anchors.right: parent.right
                    anchors.rightMargin: 20
                    anchors.verticalCenter: parent.verticalCenter

                    onPressed: root.closed();
                }
            }

            Rectangle {
                width: parent.width
                height: 1
                color: "lightgray"
            }

            SettingMenuSecion {
                height: 80
                title: qsTr("多國語言")

                LabelComboBox {
                    id: langCombobox
                    anchors.verticalCenter: parent.verticalCenter
                    width: 260
                    height: 40
                    label: qsTr("語言")
                    options: ["繁體中文", "简体中文", "English"]

                    Component.onCompleted: optionIndex = viewModel.langId;
                }

                // note: because retranslate will re-assign model to Combbox, optionIndex is unable to determinate.
                IconButton {
                    width: 80
                    height: 40
                    text: qsTr("確定")
                    icon: "pics/glyphicons-199-ok-circle.png"
                    anchors.left: langCombobox.right
                    anchors.leftMargin: 50
                    anchors.verticalCenter: parent.verticalCenter
                    backgroundColor: "lightgreen"

                    onPressed: {
                        viewModel.langId = langCombobox.optionIndex;
                        langCombobox.optionIndex = viewModel.langId;
                    }
                }
            }

            SettingMenuDbSecion {
                id: acDbSection
                height: 200
                title: qsTr("帳號資料庫")
                dbConnected: viewModel.acDbConnected

                onUpdateButtonPressed: {
                    root.restartAcDatabase(address, port, username, password, dbname);
                }
            }

            SettingMenuDbSecion {
                id: dbSection
                height: 200
                title: qsTr("內容資料庫")
                dbConnected: viewModel.dbConnected

                onUpdateButtonPressed: {
                    root.restartDatabase(address, port, username, password, dbname);
                }
            }
        }
    }
}
