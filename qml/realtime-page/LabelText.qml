import QtQuick 2.0

Item {
    implicitWidth: 160
    implicitHeight: 24

    property alias label: labelText.text
    property alias text: valueText.text

    Text {
        id: labelText
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        verticalAlignment: Text.AlignVCenter
        font.bold: true
        font.pixelSize: 16
        color: "dimgray"
    }

    Text {
        id: valueText
        anchors.left: labelText.right
        anchors.leftMargin: 18
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        verticalAlignment: Text.AlignVCenter
        font.bold: true
        font.pixelSize: 16
    }
 }
