import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {
    id: root
    property bool operating: false

    property alias bnText: bnText.text
    property alias prText: prText.text
    property alias lengthText: lengthText.text
    property alias speedText: speedText.text
    property alias remainLengthText: remainLengthText.text
    property alias timeText: timeText.text

    signal opButtonClicked();

    Rectangle {
        id: opButton
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: 10
        width: 80

        color: root.operating ? "red" : "lightgreen"

        Image {
            id: icon
            source: "../pics/glyphicons-64-power.png"
            width: 20
            height: width
            fillMode: Image.PreserveAspectFit
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: parent.height * 1 / 3 - height / 2
        }

        Text {
            text: root.operating ? qsTr("停止") : qsTr("啟動")
            font.bold: true
            font.pixelSize: 14
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: icon.bottom
            anchors.topMargin: 6
        }

        MouseArea { anchors.fill: parent; onPressed: root.opButtonClicked(); }
    }

    LabelText {
        id: bnText
        anchors.left: opButton.right
        anchors.leftMargin: 20
        anchors.top: opButton.top
        label: qsTr("批號")
        text: ""
    }

    LabelText {
        id: lengthText
        anchors.left: bnText.right
        anchors.leftMargin: 20
        anchors.top: opButton.top
        label: qsTr("米長")
        text: "N/A"
    }

    LabelText {
        id: speedText
        anchors.left: lengthText.right
        anchors.leftMargin: 20
        anchors.top: opButton.top
        label: qsTr("線速度(m/min)")
        text: "N/A"
    }

    LabelText {
        id: prText
        anchors.left: opButton.right
        anchors.leftMargin: 20
        anchors.bottom: opButton.bottom
        label: qsTr("產品")
        text: ""
    }

    LabelText {
        id: remainLengthText
        anchors.left: prText.right
        anchors.leftMargin: 20
        anchors.bottom: opButton.bottom
        label: qsTr("剩餘長度")
        text: "N/A"
    }

    LabelText {
        id: timeText
        anchors.left: remainLengthText.right
        anchors.leftMargin: 20
        anchors.bottom: opButton.bottom
        label: qsTr("預計完成時間")
        text: "00:00:00"
    }

    RectangularGlow {
        id: effect
        anchors.fill: parent
        glowRadius: 10
        color: "dimgray"
        z: -1
    }
}
