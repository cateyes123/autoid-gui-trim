import QtQuick 2.0

Item {
    id: root
    property double byteps: 25
    property int webcamLevel: 3 // note: control icons by this value

    onBytepsChanged: {
        var unit = " KB/s";
        var speed = byteps / 1024;
        if (speed > 1024) {
            unit = " MB/s";
            speed = speed / 1024;
        }

        downloadSpeed.text = speed.toFixed(2) + unit;
    }

    Image {
        id: icon
        source: "../pics/glyphicons-302-webcam.png"
        width: 14
        height: width
        fillMode: Image.PreserveAspectFit
        anchors.verticalCenter: parent.verticalCenter
    }

    Text {
        id: label
        text: qsTr("攝影機狀態")
        height: parent.height
        anchors.left: icon.right
        anchors.leftMargin: 4
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 12
        font.bold: true
        color: "dimgray"
    }

    Item {
        id: levelOuter
        anchors.left: label.right
        anchors.leftMargin: 10
        anchors.verticalCenter: parent.verticalCenter
        width: 50
        height: parent.height - 6

        property int itemWidth: 8
        property int spacing: 4

        Rectangle {
            id: level1
            anchors.bottom: parent.bottom
            width: parent.itemWidth
            height: parent.height - 2 * 3
            border.width: 1
            border.color: "gray"

            Rectangle {
                anchors.fill: parent
                visible: root.webcamLevel >= 1
                color: "limegreen"
                border.width: 1
                border.color: "gray"
            }
        }

        Rectangle {
            id: level2
            anchors.left: level1.right
            anchors.leftMargin: parent.spacing
            anchors.bottom: parent.bottom
            width: parent.itemWidth
            height: parent.height - 2 * 2
            border.width: 1
            border.color: "gray"

            Rectangle {
                anchors.fill: parent
                visible: root.webcamLevel >= 2
                color: "limegreen"
                border.width: 1
                border.color: "gray"
            }
        }

        Rectangle {
            id: level3
            anchors.left: level2.right
            anchors.leftMargin: parent.spacing
            anchors.bottom: parent.bottom
            width: parent.itemWidth
            height: parent.height - 2 * 1
            border.width: 1
            border.color: "gray"

            Rectangle {
                anchors.fill: parent
                visible: root.webcamLevel >= 3
                color: "limegreen"
                border.width: 1
                border.color: "gray"
            }
        }

        Rectangle {
            id: level4
            anchors.left: level3.right
            anchors.leftMargin: parent.spacing
            anchors.bottom: parent.bottom
            width: parent.itemWidth
            height: parent.height - 2 * 0
            border.width: 1
            border.color: "gray"

            Rectangle {
                anchors.fill: parent
                visible: root.webcamLevel >= 4
                color: "limegreen"
                border.width: 1
                border.color: "gray"
            }
        }
    }

    Text {
        id: downloadSpeed
        anchors.right: parent.right
        anchors.rightMargin: 4
        height: parent.height
        verticalAlignment: Text.AlignVCenter
        color: "gray"
        text: "25 MB/s"
    }
}
