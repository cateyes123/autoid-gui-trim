import QtQuick 2.0

Item {
    property alias title: title.text
    property alias value: value.text

    Text {
        id: title
        anchors.leftMargin: 5
        width: parent.width * 0.5
        height: parent.height
        font.pixelSize: 12
        font.bold: true
        color: "dimgray"
        horizontalAlignment: Text.AlignJustify
    }

    Text {
        id: value
        width: parent.width * 0.5 - 5
        height: parent.height
        anchors.right: parent.right
        font.pixelSize: 12
        horizontalAlignment: Text.AlignLeft
    }
}
