import QtQuick 2.0

Item {
    property alias recordModel: records

    ListModel {
        id: records

        ListElement {
            recordId: 0;
            testface: 0;
            yposition: 105;
            xposition: 135.5;
            defecttype: "smalldot";
            size: 0.34;
            width: 0.79;
            length: 0.43
            count: 2
        }

        ListElement {
            recordId: 1;
            testface: 0;
            yposition: 115;
            xposition: 105.5;
            defecttype: "smalldot";
            size: 0.34;
            width: 0.79;
            length: 0.43
            count: 2
        }

        ListElement {
            recordId: 2;
            testface: 0;
            yposition: 107;
            xposition: 130.5;
            defecttype: "smalldot";
            size: 0.34;
            width: 0.79;
            length: 0.43
            count: 2
        }

        ListElement {
            recordId: 3;
            testface: 0;
            yposition: 111;
            xposition: 145.5;
            defecttype: "smalldot";
            size: 0.34;
            width: 0.79;
            length: 0.43
            count: 2
        }

        ListElement {
            recordId: 4;
            testface: 0;
            yposition: 105;
            xposition: 106.5;
            defecttype: "smalldot";
            size: 0.34;
            width: 0.79;
            length: 0.43
            count: 2
        }
    }
}
