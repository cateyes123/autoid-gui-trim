import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4


Window {
    // the entry point of UI
    // it's dedicated for the framework of views

    id: root
    visible: true
    width: 1024
    height: 768
    title: qsTr("AutoID")

    property string pageName: ""
    property bool pageLocking: pageChanger.currentItem.item.locking

    function changePage(pageName, data) {
        var index = pageModel.indexOf(pageName);

        // check if locking
        var page = pageChanger.currentItem.item;
        if (page.locking) {
            // show popup?
        } else {
            // do change page
            pageChanger.change(index, data);
        }
    }

    Connections {
        target: settingMenu

        onReady: {
            pageChanger.model = pageModel;
            pageChanger.currentIndex = 0;
        }
    }

    ListModel {
        id: pageModel

        function indexOf(name) {
            for (var i = 0, l = count; i < l; i++) {
                if (get(i).name === name) {
                    return i;
                }
            }
            return -1;
        }

        ListElement {
            name: "LoginPage"
            src: "login-page/mainPage.qml"
        }

        ListElement {
            name: "RealtimePage"
            src: "realtime-page/mainPage.qml"
        }

        ListElement {
            name: "SelectionPage"
            src: "selection-page/mainPage.qml"
        }

        ListElement {
            name: "QueryPage"
            src: "query-page/mainPage.qml"
        }
    }

    ListView {
        id: pageChanger
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.topMargin: topControlBar.height
        orientation: ListView.Horizontal
        interactive: false
        cacheBuffer: 20
        currentIndex: -1
        spacing: 20

        property var pageData

        function change(index, data) {
            var page = pageChanger.currentItem.item;
            page.hide();
            pageChanger.pageData = data;
            pageChanger.positionViewAtIndex(index, ListView.Center);
            pageChanger.currentIndex = index;
        }

        onCurrentIndexChanged: {
            var newPage = pageChanger.currentItem.item;
            newPage.show(pageChanger.pageData);
            root.pageName = model.get(currentIndex).name;
        }

        delegate: Component {
            Loader {
               id: pageLoader
               width: pageChanger.width
               height: pageChanger.height
               source: src

               onStatusChanged: {
                   if (status == Loader.Ready) {
                   }
               }

               Connections {
                   target: item

                   onChangePageRequest: {
                       root.changePage(pageName, data);
                   }

                   onPopupRequest: {
                       popup.pop(message, description);
                   }
               }
            }
        }
    }

    ControlBar {
        id: topControlBar
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: 36
        currentPageName: root.pageName
        currentPageLocking: root.pageLocking

        onSettingClicked: settingMenu.visible = true;
        onChangePageRequested: root.changePage(pageName);
    }

    SettingMenu {
        id: settingMenu
        anchors.fill: parent
        contentWidth: 800
        contentHeight: 600
        visible: false

        onClosed: visible = false;
        onTipsPopped: popup.pop(message, description);
    }

    Popup {
        id: popup
        anchors.fill: parent
        visible: false
        z: 5

        onClosed: visible = false;
    }
}
