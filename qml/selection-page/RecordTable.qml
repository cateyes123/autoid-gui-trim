import QtQuick 2.0
import QtQuick.Controls 1.4


TableView {

    TableViewColumn {
        role: "bn"
        title: qsTr("批號")
        width: 100
    }
    TableViewColumn {
        role: "pr"
        title: qsTr("產品")
        width: 100
    }
    TableViewColumn {
        role: "len"
        title: qsTr("寬幅")
        width: 100
    }
    TableViewColumn {
        role: "id"
        title: qsTr("操作員")
        width: 100
    }
    TableViewColumn {
        role: "customid"
        title: qsTr("客戶名稱")
        width: 100
    }
    TableViewColumn {
        role: "notes"
        title: qsTr("備註")
        width: 100
    }
}
