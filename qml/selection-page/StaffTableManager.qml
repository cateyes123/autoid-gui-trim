import QtQuick 2.0
import QtGraphicalEffects 1.0
import "../"

Rectangle {
    id: root
    anchors.fill: parent
    color: Qt.rgba(0, 0, 0, 0.25)

    signal closed();

    property alias model: table.model
    property string title: qsTr("員工管理")
    property int selectedStaffId: 0

    function currentItem() {
        return qsTr("員工代號") + ": " + root.selectedStaffId;
    }

    function setData(data) {
        root.selectedStaffId = data.staffId;
        staffNameInput.text = data.staffName;
        staffGroupIdInput.text = data.staffGroupId;
    }

    function getData() {
        var data = {};
        data.staffId = root.selectedStaffId;
        data.staffName = staffNameInput.text;
        data.staffGroupId = staffGroupIdInput.text;
        return data;
    }

    function performTableSelect(rowIndex) {
        var data = model.get(rowIndex);
        if (data) {
            setData(data);
        }
    }

    function performNew() {
        var data = getData();
        var success = model.insertDb(data.staffName, data.staffGroupId);
        if (success) {
            model.refresh();
        } else {
            dbStatusPopup.pop(qsTr("失敗"), model.lastDbError());
        }
    }

    function performEdit() {
        var data = getData();
        var success = model.updateDb(data.staffId, data.staffName, data.staffGroupId);
        if (success) {
            model.refresh();
        } else {
            dbStatusPopup.pop(qsTr("失敗"), model.lastDbError());
        }
    }

    function performDelete() {
        var data = getData();
        var success = model.deleteDb(data.staffId);
        if (success) {
            table.selection.clear();
            model.refresh();
        } else {
            dbStatusPopup.pop(qsTr("失敗"), model.lastDbError());
        }
    }

    MouseArea {
        anchors.fill: parent
    }

    RectangularGlow {
        id: effect
        anchors.fill: content
        glowRadius: 10
        color: "#2f2f2f"
    }

    Rectangle {
        id: content
        anchors.centerIn: parent
        width: 800
        height: 600

        Item {
            anchors.fill: parent
            anchors.margins: 8

            Text {
                id: titleText
                height: 30
                width: parent.width / 2
                text: root.title
                font.pixelSize: 18
                font.bold: true
            }

            IconButton {
                width: 80
                height: 30
                text: qsTr("關閉")
                icon: "../pics/glyphicons-198-remove-circle.png"
                anchors.top: parent.top
                anchors.right: parent.right

                onPressed: root.closed();
            }

            StaffTable {
                id: table
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height - titleText.height - fields.height - buttons.height

                Connections {
                    target: table.selection
                    onSelectionChanged: {
                        table.selection.forEach(function (rowIndex) {
                            root.performTableSelect(rowIndex);
                        })
                    }
                }
            }

            Row {
                id: fields
                anchors.top: table.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.topMargin: 20
                height: 64
                spacing: 10

                LabelTextInput {
                    id: staffNameInput
                    label: qsTr("員工名稱")
                    width: 200
                    height: 40
                    maximumLength: 50
                }

                LabelTextInput {
                    id: staffGroupIdInput
                    label: qsTr("員工部門")
                    width: 120
                    height: 40
                    maximumLength: 20
                }
            }

            Row {
                id: buttons
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: fields.bottom
                height: 50
                spacing: 8

                IconButton {
                    width: 160
                    height: 24
                    backgroundColor: "lightblue"
                    text: qsTr("新增")
                    icon: "../pics/glyphicons-199-ok-circle.png"
                    onPressed: popup.popConfirm(qsTr("新增"), "", root.performNew)
                }

                IconButton {
                    width: 160
                    height: 24
                    backgroundColor: "lightblue"
                    text: qsTr("修改")
                    icon: "../pics/glyphicons-199-ok-circle.png"
                    onPressed: if (table.selection.count === 1) popup.popConfirm(qsTr("修改"), root.currentItem(), root.performEdit)
                }

                IconButton {
                    width: 160
                    height: 24
                    backgroundColor: "lightblue"
                    text: qsTr("刪除")
                    icon: "../pics/glyphicons-198-remove-circle.png"
                    onPressed: if (table.selection.count === 1) popup.popConfirm(qsTr("刪除"), root.currentItem(), root.performDelete)
                }
            }
        }
    }

    Popup {
        id: popup
        anchors.fill: parent
        visible: false

        onClosed: visible = false
    }

    Popup {
        id: dbStatusPopup
        anchors.fill: parent
        visible: false

        onClosed: visible = false
    }
}
