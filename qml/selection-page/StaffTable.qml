import QtQuick 2.0
import QtQuick.Controls 1.4


TableView {
    selectionMode: SelectionMode.SingleSelection

    TableViewColumn {
        role: "staffId"
        title: qsTr("員工代號")
        width: 100
    }
    TableViewColumn {
        role: "staffName"
        title: qsTr("員工名稱")
        width: 160
    }
    TableViewColumn {
        role: "staffGroupId"
        title: qsTr("員工部門")
        width: 100
    }
}
