import QtQuick 2.0
import com.autoid 1.0
import "../"

Page {
    id: root

    function show(data) {
        root.locking = true;

        viewModel.refresh();
    }

    function showRecord(record) {
        bnInput.text = record.bn;
        prInput.text = record.pr;
        lenInput.text = record.len;
        staffIdInput.selectId(record.id);
        var customIndex = viewModel.customModel.indexFromId(record.customid);
        if (customIndex >= 0) {
            customTable.selection.select(customIndex, customIndex);
            customTable.currentRow = customIndex;
        } else {
            customTable.selection.clear();
            customTable.currentRow = -1;
        }

        notesInput.text = record.notes;
    }

    SelectionViewModel {
        id: viewModel

        Component.onCompleted: {
        }
    }

    Section {
        id: inputSection
        anchors.left: parent.left
        anchors.top: parent.top
        width: 300
        height: 260

        function collectData() {
            var result = {};

            result.bn = bnInput.text;
            result.pr = prInput.text;
            result.len = lenInput.text;
            result.staffId = viewModel.staffModel.get(staffIdInput.optionIndex).staffId;
            result.customId = viewModel.customModel.get(customTable.currentRow).customId;
            result.notes = notesInput.text;

            return result;
        }

        Column {
            id: recordEditGroup
            anchors.fill: parent
            anchors.margins: 8
            spacing: 4

            LabelTextInput {
                id: bnInput
                width: parent.width
                height: 40
                label: qsTr("批號")
                text: ""

                KeyNavigation.tab: prInput
            }

            LabelTextInput {
                id: prInput
                width: parent.width
                height: 40
                label: qsTr("產品")
                text: ""

                KeyNavigation.tab: lenInput
            }

            LabelTextInput {
                id: lenInput
                width: parent.width
                height: 40
                label: qsTr("幅寬")
            }

            LabelComboBox {
                id: staffIdInput
                width: parent.width
                height: 40
                label: qsTr("操作員")
                options: viewModel.staffModel

                function selectId(id) {
                    optionIndex = viewModel.staffModel.indexFromId(id);
                }

                Image {
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.topMargin: 4
                    anchors.rightMargin: 6
                    width: 12
                    height: width
                    fillMode: Image.PreserveAspectFit
                    source: "../pics/glyphicons-281-settings.png"

                    MouseArea {
                        anchors.fill: parent
                        onPressed: staffTableManager.visible = true
                    }
                }
            }

            LabelTextInput {
                id: notesInput
                width: parent.width
                height: 86
                label: qsTr("備註")
                inputHeight: 46
                maximumLength: 50
            }
        }
    }

    Section {
        anchors.top: inputSection.bottom
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        width: inputSection.width

        Column {
            anchors.fill: parent
            anchors.margins: 8
            spacing: 8

            Text {
                id: customTitle
                width: parent.width

                text: qsTr("廠商")
                font.pixelSize: 12
                font.bold: true
                color: "dimgray"

                Image {
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.topMargin: 4
                    anchors.rightMargin: 6
                    width: 12
                    height: width
                    fillMode: Image.PreserveAspectFit
                    source: "../pics/glyphicons-281-settings.png"

                    MouseArea {
                        anchors.fill: parent
                        onPressed: customTableManager.visible = true
                    }
                }
            }

            CustomTable {
                id: customTable
                model: viewModel.customModel
                width: parent.width
                height: parent.height - customTitle.height - 8
            }
        }
    }

    Section {
        id: recordSection
        anchors.left: inputSection.right
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: buttonSection.top

        Column {
            anchors.fill: parent
            anchors.margins: 8
            spacing: 8

            Text {
                id: recordTitle
                width: parent.width

                text: qsTr("歷史紀錄")
                font.pixelSize: 12
                font.bold: true
                color: "dimgray"
            }

            RecordTable {
                id: recordTable
                width: parent.width
                height: parent.height - customTitle.height - 8
                focus: true
                model: viewModel.recordModel

                onCurrentRowChanged: {
                    var record = viewModel.getRecord(currentRow);
                    root.showRecord(record);
                }

                onSortIndicatorColumnChanged: sortColumn();
                onSortIndicatorOrderChanged: sortColumn();

                function sortColumn() {
                    sortIndicatorVisible = true;
                    viewModel.sortRecords(sortIndicatorColumn, sortIndicatorOrder == 0 ? 1 : 0);
                }
            }
        }
    }

    Section {
        id: buttonSection
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        width: recordSection.width
        height: 40

        Row {
            anchors.fill: parent
            anchors.margins: 8
            spacing: 8

            IconButton {
                width: parent.width / 2 - 4
                height: parent.height
                backgroundColor: "lightgreen"
                text: qsTr("確認執行")
                icon: "../pics/glyphicons-199-ok-circle.png"

                onPressed: {
                    if (customTable.currentRow >= 0) {
                        var data = inputSection.collectData();
                        viewModel.saveRecord(data);
                        root.locking = false;
                        root.changePageRequest("RealtimePage", data);

                    } else {
                        root.popupRequest(qsTr("請選擇廠商"), "");
                    }
                }
            }

            IconButton {
                width: parent.width / 2 - 4
                height: parent.height
                backgroundColor: "red"
                text: qsTr("離開頁面")
                icon: "../pics/glyphicons-198-remove-circle.png"

                onPressed: {
                    root.locking = false;
                    root.changePageRequest("RealtimePage", null);
                }
            }
        }
    }

    StaffTableManager {
        id: staffTableManager
        anchors.fill: parent
        visible: false
        model: viewModel.staffModel

        onClosed: visible = false
    }

    CustomTableManager {
        id: customTableManager
        anchors.fill: parent
        visible: false
        model: viewModel.customModel

        onClosed: visible = false
    }
}
