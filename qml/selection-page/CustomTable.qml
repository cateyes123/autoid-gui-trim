import QtQuick 2.0
import QtQuick.Controls 1.4


TableView {
    selectionMode: SelectionMode.SingleSelection

    TableViewColumn {
        role: "customId"
        title: qsTr("客戶代號")
        width: 100
    }
    TableViewColumn {
        role: "customName"
        title: qsTr("客戶名稱")
        width: 160
    }
}
