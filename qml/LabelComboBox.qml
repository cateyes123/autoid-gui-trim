import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    id: root
    property alias label: label.text
    property alias labelHAlignment: label.horizontalAlignment
    property alias options: comboBox.model
    property alias optionIndex: comboBox.currentIndex
    signal activated(int index);

    Column {
        anchors.fill: parent
        spacing: 2

        Text {
            id: label
            anchors.left: parent.left
            anchors.right: parent.right
            horizontalAlignment: Text.AlignLeft

            text: "Label"
            font.pixelSize: 12
            font.bold: true
            color: "dimgray"
        }

        ComboBox {
            id: comboBox
            anchors.left: parent.left
            anchors.right: parent.right
            //model: [ "Banana", "Apple", "Coconut" ]
            onActivated: root.activated(index)
        }
    }
}


