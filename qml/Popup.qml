import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {
    id: root

    signal closed();

    property alias contentWidth: content.width
    property alias contentHeight: content.height
    property var confirmHandler: null

    function pop(message, description) {
        messageText.text = message;
        descriptionText.text = description;
        confirmHandler = null;
        visible = true;
    }

    function popConfirm(message, description, confirmCallback) {
        messageText.text = message;
        descriptionText.text = description;
        confirmHandler = confirmCallback;
        visible = true;
    }

    color: Qt.rgba(0, 0, 0, 0.25)

    MouseArea {
        anchors.fill: parent
    }

    RectangularGlow {
        id: effect
        anchors.fill: content
        glowRadius: 10
        color: "#2f2f2f"
    }

    Rectangle {
        id: content
        anchors.centerIn: parent
        width: 460
        height: 300

        Column {
            anchors.fill: parent

            Item {
                id: titleGroup
                width: parent.width
                height: 50

                Text {
                    text: qsTr("提示")
                    anchors.centerIn: parent
                    font.pixelSize: 18
                    font.bold: true
                }

                IconButton {
                    width: 80
                    height: 30
                    text: qsTr("關閉")
                    icon: "pics/glyphicons-198-remove-circle.png"
                    anchors.right: parent.right
                    anchors.rightMargin: 20
                    anchors.verticalCenter: parent.verticalCenter

                    onPressed: root.closed();
                }
            }

            Rectangle {
                width: parent.width
                height: 1
                color: "lightgray"
            }


            Item {
                width: parent.width
                height: 250

                Column {
                    anchors.fill: parent
                    anchors.margins: 10
                    spacing: 10

                    Text {
                        id: messageText
                        width: parent.width
                        height: 24
                        text: "Message"
                        font.pixelSize: 18
                        horizontalAlignment: Text.AlignHCenter
                    }

                    Text {
                        id: descriptionText
                        width: parent.width
                        height: confirmRow.visible ? 160 : 200
                        text: "This is a description."
                        horizontalAlignment: Text.AlignJustify
                        wrapMode: Text.Wrap
                        color: "gray"
                    }

                    Row {
                        id: confirmRow
                        width: parent.width
                        visible: root.confirmHandler
                        height: 30
                        spacing: 12

                        IconButton {
                            width: (parent.width - parent.spacing) / 2
                            height: 24
                            backgroundColor: "lightgreen"
                            text: qsTr("確認")
                            icon: "pics/glyphicons-199-ok-circle.png"
                            onPressed: {
                                root.confirmHandler();
                                root.confirmHandler = null;
                                root.closed();
                            }
                        }

                        IconButton {
                            width: (parent.width - parent.spacing) / 2
                            height: 24
                            backgroundColor: "red"
                            text: qsTr("取消")
                            icon: "pics/glyphicons-198-remove-circle.png"
                            onPressed: root.closed();
                        }
                    }
                }
            }
        }
    }
}
