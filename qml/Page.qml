import QtQuick 2.0

Rectangle {
    color: "#dcdcdc"
    implicitWidth: 1024
    implicitHeight: 768

    property bool locking: false

    signal changePageRequest(string pageName, var data)
    signal popupRequest(string message, string description)

    function show(data) {
        // override
    }

    function hide() {
        // override
    }

    function refresh() {
        // override
    }
}
