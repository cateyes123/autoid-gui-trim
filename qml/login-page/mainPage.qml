import QtQuick 2.0
import QtGraphicalEffects 1.0
import com.autoid 1.0
import "../"

Page {
    id: root
    locking: true

    LoginViewModel {
        id: viewModel

        Component.onCompleted: {
            usernameInput.text = username;
        }
    }

    RectangularGlow {
        id: effect
        anchors.fill: content
        glowRadius: 10
        color: "#2f2f2f"
    }

    Section {
        id: content
        width: 400
        height: 400
        anchors.centerIn: parent

        Item {
            anchors.fill: parent
            anchors.margins: 16

            Text {
                width: parent.width
                height: 50
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 40
                text: qsTr("登入")
            }


            LabelTextInput {
                id: usernameInput
                width: parent.width
                anchors.top: parent.top
                anchors.topMargin: 80
                label: qsTr("帳號")
                text: ""
                focus: true

                KeyNavigation.tab: passwordInput
            }

            LabelTextInput {
                id: passwordInput
                width: parent.width
                anchors.top: usernameInput.bottom
                anchors.topMargin: 10
                echoMode: TextInput.Password
                label: qsTr("密碼")
                text: ""

                KeyNavigation.tab: usernameInput
            }

            IconButton {
                anchors.bottom: parent.bottom
                width: parent.width
                height: 30
                backgroundColor: "lightgreen"
                text: qsTr("確認")
                icon: "../pics/glyphicons-199-ok-circle.png"

                onPressed: {
                    var username = usernameInput.text;
                    var password = passwordInput.text;
                    var message = "";
                    var description = "";

                    var dbConnected = viewModel.dbConnected();
                    if (viewModel.dbConnected()) {
                        var pass = viewModel.verify(username, password);
                        if (pass) {
                            root.locking = false;
                            root.changePageRequest("RealtimePage", null);
                        } else {
                            message = qsTr("帳號密碼錯誤");
                            description = qsTr("請聯絡管理員");
                            root.popupRequest(message, description);
                        }
                    } else {
                        message = qsTr("資料庫錯誤");
                        description = viewModel.dbLastError();
                        root.popupRequest(message, description);
                    }

                    // write back for storing
                    viewModel.username = username;
                }
            }
        }
    }
}
