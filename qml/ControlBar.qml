import QtQuick 2.0

Rectangle {
    id: root
    color: Qt.rgba(0, 0, 0)

    property var currentPageName
    property bool currentPageLocking: false

    signal changePageRequested(string pageName);
    signal settingClicked();

    IconButton {
        readonly property string name: "RealtimePage"

        id: pageButtonRealtime
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.verticalCenter: parent.verticalCenter
        width: 120
        height: parent.height
        text: qsTr("即時瑕疵資料")
        textColor: currentPageName === name ? "white" : "gray"
        textBold: true
        backgroundColor: "transparent"
        icon: ""

        onPressed: changePageRequested(name);

        opacity: root.currentPageLocking ? (root.currentPageName === name ? 1 : 0) : 1
        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
    }

    IconButton {
        readonly property string name: "SelectionPage"

        id: pageButtonSelection
        anchors.left: parent.left
        anchors.leftMargin: 120 * 1
        anchors.verticalCenter: parent.verticalCenter
        width: 120
        height: parent.height
        text: qsTr("驗布資料選擇")
        textColor: currentPageName === name ? "white" : "gray"
        textBold: true
        backgroundColor: "transparent"
        icon: ""

        onPressed: changePageRequested(name);

        opacity: root.currentPageLocking ? (root.currentPageName === name ? 1 : 0) : 1
        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
    }

    IconButton {
        readonly property string name: "QueryPage"

        id: pageButtonQuery
        anchors.left: parent.left
        anchors.leftMargin: 120 * 2
        anchors.verticalCenter: parent.verticalCenter
        width: 120
        height: parent.height
        text: qsTr("檢測履歷查詢")
        textColor: currentPageName === name ? "white" : "gray"
        textBold: true
        backgroundColor: "transparent"
        icon: ""

        onPressed: changePageRequested(name);

        opacity: root.currentPageLocking ? (root.currentPageName === name ? 1 : 0) : 1
        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
    }

    IconButton {
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        height: parent.height
        text: qsTr("設定")
        textColor: "white"
        textBold: true
        backgroundColor: "transparent"
        icon: "pics/glyphicons-281-settings-white.png"

        onPressed: root.settingClicked();
    }
}
