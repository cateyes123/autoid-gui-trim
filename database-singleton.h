#ifndef DATABASESINGLETON_H
#define DATABASESINGLETON_H

#include <QObject>
#include <QtSql>


// dedictaed to connections fo database and obtaining the status
class DatabaseSingleton : public QObject, public QSqlDatabase
{
    Q_OBJECT

public:
    static DatabaseSingleton & instance();

private:
    DatabaseSingleton() = delete;

protected:
    DatabaseSingleton(const QSqlDatabase & other);

signals:
    void connectedChanged();

public:
    bool connected() const { return connected_; }

    bool open(const QString & address, const int port, const QString & username,  const QString & password,  const QString & dbname);

private:
    void setConnected(bool set);

private:
    bool connected_;
};

#endif // DATABASESINGLETON_H
