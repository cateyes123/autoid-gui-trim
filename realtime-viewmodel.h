#ifndef REALTIMEVIEWMODEL_H
#define REALTIMEVIEWMODEL_H

#include <QObject>
#include <QVariantMap>
#include <QString>
#include <network-monitor.h>


class QAbstractItemModel;
class QStandardItemModel;


// view model for realtime page
class RealtimeViewModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QAbstractItemModel * drecordModel READ drecordModel CONSTANT)
    Q_PROPERTY(QString defectImgFolder READ defectImgFolder CONSTANT)
    Q_PROPERTY(double networkTx READ networkTx CONSTANT)
    Q_PROPERTY(double networkRx READ networkRx CONSTANT)

public:
    RealtimeViewModel(QObject * parent = nullptr);
    virtual ~RealtimeViewModel() {}

public:
    QAbstractItemModel * drecordModel() const;
    QString defectImgFolder() const;
    double networkTx() const { return networkResult_.tx; }
    double networkRx() const { return networkResult_.rx; }

    Q_INVOKABLE QVariantMap getDefectRecord(int index) const;
    Q_INVOKABLE QVariantMap queryChartData(double fromY, double toY);
    Q_INVOKABLE void refreshDb();
    Q_INVOKABLE void clearDb();

private:
    void emitSqlFailed(const QString & reason) { emit sqlFailed(reason); }

signals:
    void networkStatusUpdated();
    void sqlFailed(QString reason);

private slots:
    void handleNetworkMonitorUpdated(const NetworkResult &);

private:
    QStandardItemModel * drecordModel_;
    NetworkMonitor * networkMonitor_;
    NetworkResult networkResult_;
};

#endif // REALTIMEVIEWMODEL_H
