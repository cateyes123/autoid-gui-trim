#include "database-singleton.h"

DatabaseSingleton & DatabaseSingleton::instance()
{
    static DatabaseSingleton inst(QSqlDatabase::addDatabase("QMYSQL"));
    return inst;
}

DatabaseSingleton::DatabaseSingleton(const QSqlDatabase & other)
    : QObject(nullptr)
    , QSqlDatabase(other)
    , connected_(false)
{
}

bool DatabaseSingleton::open(const QString & address, const int port, const QString & username,  const QString & password,  const QString & dbname)
{
    setHostName(address);
    setPort(port);
    setDatabaseName(dbname);
    bool opened = QSqlDatabase::open(username, password);
    setConnected(opened);
    return opened;
}

void DatabaseSingleton::setConnected(bool set)
{
    if (connected_ != set)
    {
        connected_ = set;
        emit connectedChanged();
    }
}
